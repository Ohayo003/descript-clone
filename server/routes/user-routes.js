import express from "express";
import {
  userLogin,
  create_user_account,
} from "../Controllers/user-controller.js";

const route = express.Router();

route.post("/login", userLogin);

//Create account
route.post("/create-account", create_user_account);

export default route;
