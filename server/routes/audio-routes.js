import express from "express";
import {
  create_trained_folder,
  get_audio_sounds,
  get_existing_audio_by_name,
  train_new_audio,
  delete_existing_audio,
} from "../Controllers/existing-audio-controllers.js";
import { upload } from "../multer.js";

const audio = express.Router();

audio.post("/create/audio-training-folder", create_trained_folder);

audio.post("/audio/:userId", upload.single("audioFile"), train_new_audio);

audio.get("/get-audio/:userId", get_existing_audio_by_name);

audio.get("/get-audio-sounds/:userId", get_audio_sounds);

audio.delete("/delete-audio/:userId", delete_existing_audio);

export default audio;
