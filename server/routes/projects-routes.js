import express from "express";
import { upload } from "../multer.js";
import {
  getUsers_project,
  upload_file,
  create_project_folder,
  delete_projects,
  upload_video_recordings,
  transcript_media_file,
  getProject_byId,
  get_video_recordings,
  get_transcript,
  upload_podcasting_recordings,
  use_media_from_workspace,
} from "../Controllers/projects-controller.js";

const projects = express.Router();

//Upload file
projects.post("/create-project-folder", create_project_folder);

//Get projects of users
projects.get("/user-projects/:userID", getUsers_project);

//Delete files
projects.delete("/delete-projects/:userId/:projectID", delete_projects);

//Upload files to cloud storage
projects.put(
  "/file-upload/transcript-video/:userId/:projectID",
  upload.single("media"),
  upload_file
);

//Get project data by Id
projects.get("/get-project/:projectID", getProject_byId);

//Transcribe audio or video
projects.put(
  "/file-upload/transcribe-audio-video/:projectID",
  transcript_media_file
);

//Get transcript file
projects.get("/", get_transcript);

//Upload video recordings
projects.put(
  "/file-upload/video-recordings/:userId",
  upload.single("Recordings"),
  upload_video_recordings
);

projects.post(
  "/file-upload/podcasting-recordings/:userId",
  upload.single("Podcasting"),
  upload_podcasting_recordings
);

projects.get("/files/get-video-recordings/:userId", get_video_recordings);

projects.put("/use-existing-recording/:userId", use_media_from_workspace);

export default projects;
