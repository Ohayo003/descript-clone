import express from "express";
import {
  addNewAudio,
  deleteAudio,
  ReplaceExistingWord,
  undo_redo_changes,
} from "../Controllers/edit-controllers.js";
import { upload } from "../multer.js";

const edit = express.Router();

edit.put(
  "/delete-audio/:projectId",
  upload.fields([{ name: "original" }, { name: "transcriptData" }]),
  deleteAudio
);

edit.put(
  "/replace-word/:userId/:projectId",
  upload.fields([
    { name: "original" },
    { name: "newAudio" },
    { name: "transcriptData" },
    { name: "existingAudio" },
  ]),
  ReplaceExistingWord
);

edit.put(
  "/add-new-audio/:projectId",
  upload.fields([
    { name: "original" },
    { name: "newAudio" },
    { name: "transcriptData" },
    { name: "existingAudio" },
  ]),
  addNewAudio
);

edit.put(
  "/changes/undo-redo-changes/:projectId",
  upload.fields([{ name: "audio" }, { name: "transcriptData" }]),
  undo_redo_changes
);
export default edit;
