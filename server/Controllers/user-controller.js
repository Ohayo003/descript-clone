import userModel from "../models/user-model.js";
import bCrypt from "bcrypt";

const saltRounds = 10;

export const userLogin = async (req, res) => {
  const { email, password } = req.body;
  let getUser;
  try {
    if (email) {
      await userModel
        .findOne({ email: email })
        .exec()
        .then(async (result) => {
          if (!result) {
            return res.status(400).json({
              message: "account does not exist!",
            });
          }
          result.status = 200;
          if (result.status === 200) {
            getUser = result;
            if (password) {
              await bCrypt
                .compare(password, getUser.password)
                .then((matched) => {
                  if (!matched) {
                    return res.status(400).json({
                      message: "Invalid Email or Password!",
                    });
                  }
                  return res.status(202).json(getUser);
                });
            }
          }
        });
    } else {
      return res.status(400).json({
        message: "Please Provide Email and Password",
      });
    }
  } catch (error) {
    return res.status(400).send({
      message: error,
    });
  }
};

export const create_user_account = async (req, res) => {
  const { firstName, lastName, email, password, gender } = req.body;
  try {
    await bCrypt.hash(password, saltRounds).then(async (hashed) => {
      const userData = new userModel({
        firstName: firstName,
        lastName: lastName,
        gender: gender,
        email: email.toLowerCase(),
        password: hashed,
      });
      await userModel.findOne({ email: email }).then(async (result) => {
        if (result) {
          return res.status(302).json({
            message: "email already exist",
          });
        }
        if (!result) {
          userData.save().then((resultData) => {
            if (!resultData) {
              res.status(400).json({
                message: "Failed to add user data",
              });
            }
            return res.status(201).json(resultData);
          });
        }
      });
    });
  } catch (error) {
    console.log(error);
  }
};
