import projectSchema from "../models/project-files-model.js";
import fs from "fs";
import DataURIParser from "datauri/parser.js";
import path from "path";
import textToSpeech from "@google-cloud/text-to-speech";
import cloudinaryStorage from "../cloudinaryConfig.js";
import getMp3Duration from "get-mp3-duration";
import { promisify } from "util";
import {
  addNewAudioCut,
  audioCut,
  extractAudioFromVideo,
  removeAudio,
  ReplaceExisting,
} from "../audio-editing-helper.js";

const parser = new DataURIParser();

const __dirname = path.resolve();

const writeFile = promisify(fs.writeFile);

const unlink = promisify(fs.unlink);
const mkdir = promisify(fs.mkdir);

const textSpeechClient = new textToSpeech.TextToSpeechClient({
  keyFile: process.env.GOOGLE_ACCOUNT_CREDENTIALS,
  projectId: "text-to-speech-329005",
});

let uniqueID = () => {
  let ID = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  };

  return `${ID() + ID()}-${ID()}-${ID() + ID()}`;
};

export function formatTime(seconds = 0) {
  const minutes = Math.floor((seconds % 3600) / 60)
    .toString()
    .padStart(2, "0");
  const newSeconds = (seconds % 60).toFixed(3).toString().padStart(6, "0");
  const hours = Math.floor(seconds / 3600)
    .toString()
    .padStart(2, "0");
  return `${hours || "00"}:${minutes || "00"}:${
    newSeconds || seconds.toString().padStart(6, "0")
  }`;
}

//Create Directories
async function directoryFile() {
  await mkdir(`${__dirname}/public/editing/new-audio-word/`, {
    recursive: true,
  });
  await mkdir(`${__dirname}/public/Original Audio/`, {
    recursive: true,
  });
  await mkdir(`${__dirname}/public/editing/new-output/`, {
    recursive: true,
  });
  await mkdir(`${__dirname}/public/editing/cut-audio/`, {
    recursive: true,
  });
  await mkdir(`${__dirname}/public/temp/`, { recursive: true });
}

directoryFile();

//Replace word and audio
export const ReplaceExistingWord = async (req, res) => {
  const { newWord, start, end, index, gender } = req.body;
  const { userId, projectId } = req.params;
  let name, fileName, jsonData, success, newEnd;
  try {
    // const user = await userModel.findOne({ userId }).exec();
    const project = await projectSchema.findOne({ _id: projectId }).exec();

    project.files.media.map((media) => {
      name = media.name;
    });
    project.files.transcription.map((file) => {
      fileName = file.name;
    });

    const duration = getMp3Duration(req.files.newAudio[0].buffer);

    newEnd = start + duration;

    console.log(fileName);
    console.log("start: ", start);
    console.log("end: ", end);
    console.log(newWord);

    const projectName = project.projectName.split(" ").join("");

    const transcriptPath = `${__dirname}/public/temp/${projectName}-transcript-file.json`;

    const originalAudioPath = `${__dirname}/public/Original Audio/${projectName}-original-audio.mp3`;

    const localVoicePath = `${__dirname}/public/editing/new-audio-word/${newWord}.mp3`;

    // const newPath = `${__dirname}/public/editing/new-output/`;

    fs.writeFileSync(
      transcriptPath,
      Buffer.from(new Uint8Array(req.files.transcriptData[0].buffer))
    );
    console.log(`transPath - ${transcriptPath}`);
    console.log(index);
    var jsonFile = fs.readFileSync(transcriptPath, "utf8");
    jsonData = JSON.parse(jsonFile);

    console.log("current value: ", jsonData[0].elements[index].value);

    jsonData[0].elements[index].value = ` ${newWord} `;
    // jsonData[0].elements[index].ts = start;
    jsonData[0].elements[index].end_ts = newEnd;
    // jsonData[0].elements[index].confidence = 1;

    console.log("new value : ", jsonData[0].elements[index].value);
    console.log("new ts : ", jsonData[0].elements[index].end_ts);
    console.log("new end_ts : ", jsonData[0].elements[index].ts);
    console.log("new confidence : ", jsonData[0].elements[index].confidence);

    await writeFile(transcriptPath, JSON.stringify(jsonData));

    console.log(`${transcriptPath} is updated`);

    // console.log("new data", jsonFile[0].elements[index].value);

    console.log(formatTime(duration));

    // const request = {
    //   input: { text: `${newWord}` },
    //   voice: { languageCode: "en-US", ssmlGender: `${gender}` },
    //   audioConfig: { audioEncoding: "MP3" },
    // };
    // const [response] = await textSpeechClient.synthesizeSpeech(request);
    // await writeFile(localVoicePath, response.audioContent, "binary");

    fs.writeFileSync(
      localVoicePath,
      Buffer.from(new Uint8Array(req.files.newAudio[0].buffer))
    );
    console.log(`${localVoicePath} Created!`);

    fs.writeFileSync(
      originalAudioPath,
      Buffer.from(new Uint8Array(req.files.original[0].buffer))
    );

    const output = await ReplaceExisting(
      originalAudioPath,
      localVoicePath,
      newWord,
      start,
      duration,
      end,
      projectName
    );

    // console.log(`output - ${output}`);

    await cloudinaryStorage.uploader
      .upload(output, {
        public_id: `${projectName}-${uniqueID()}-updated-replaced`,
        resource_type: "auto",
        use_filename: true,
        folder: "Videos/Editing/Replaced",
      })
      .then(async (result) => {
        if (result) {
          console.log("file Uploaded!");
          await projectSchema
            .findOneAndUpdate(
              {
                _id: project._id,
                "files.media.name": name,
              },
              {
                $set: { "files.media.$.url": result.secure_url },
              },
              { new: true }
            )
            .exec()
            .then(async (result) => {
              if (result) {
                console.log(result);
                await unlink(localVoicePath);
                await unlink(originalAudioPath);
                await unlink(output);
                return res.status(200).json(result);
              } else {
                success = false;
                console.error;
                return res.status(400).json({
                  status: "failed",
                  message: "Updated Failed",
                });
              }
            })
            .catch((error) => console.error(error));
        }
      });

    //upload the updated transcript file
    await cloudinaryStorage.uploader
      .upload(transcriptPath, {
        public_id: `${projectName}-transcript-file`,
        resource_type: "auto",
        use_filename: true,
        folder: "Videos/Transcriptions/Transcription-Files",
        overwrite: true,
      })
      .then(async (result) => {
        if (result) {
          await projectSchema
            .findOneAndUpdate(
              {
                _id: project._id,
                "files.transcription.name": fileName,
              },
              {
                $set: { "files.transcription.$.url": result.secure_url },
              }
            )
            .exec()
            .then(async (result) => {
              if (result) {
                await unlink(transcriptPath);
                console.log(`${projectName} transcript file updated`);
              }
            });
        }
      });
  } catch (error) {
    console.log(error);
  }
};

//Delete Audio and Word
export const deleteAudio = async (req, res) => {
  // const { url } = req.body;
  const { projectId } = req.params;
  const { start, end, index, word, id } = req.body;
  let name, fileName, jsonData, success;
  try {
    req.setTimeout(6000 * 1000);

    const project = await projectSchema.findOne({ _id: projectId }).exec();
    project.files.media.map((media) => {
      name = media.name;
    });
    project.files.transcription.map((file) => {
      fileName = file.name;
    });

    const projectName = project.projectName.split(" ").join("");
    const transcriptPath = `${__dirname}/public/temp/${projectName}-transcript-file.json`;

    const originalAudio = `${__dirname}/public/Original Audio/${projectName}-original.mp3`;

    const newLocation = `${__dirname}/public/editing/cut-audio/${projectName}-cutted-output.mp3`;
    // const newPath = `/${__dirname}/public/editing/cut-audio/${
    //   project.projectName
    // }-${uniqueID()}-Output.mp3`;

    console.log(req.file);

    fs.writeFileSync(
      transcriptPath,
      Buffer.from(new Uint8Array(req.files.transcriptData[0].buffer))
    );
    console.log(index);
    var jsonFile = fs.readFileSync(transcriptPath, "utf8");
    jsonData = JSON.parse(jsonFile);

    console.log("current value: ", jsonData[0].elements[index].value);

    jsonData[0].elements.splice(index, 1);
    console.log("new value : ", jsonData[0].elements[index].value);

    await writeFile(transcriptPath, JSON.stringify(jsonData));

    console.log(`${transcriptPath} is updated`);

    fs.writeFileSync(
      originalAudio,
      Buffer.from(new Uint8Array(req.files.original[0].buffer))
    );
    const output = await removeAudio(originalAudio, start, end, projectName);

    // const extName = path.extname(newPath).toString();
    // const file64 = parser.format(extName, req.file.buffer);

    //Upload the new media
    await cloudinaryStorage.uploader
      .upload(output, {
        public_id: `${project.projectName}-${uniqueID()}-updated`,
        resource_type: "auto",
        use_filename: true,
        folder: "Videos/Editing/Deleted",
      })
      .then(async (result) => {
        if (result) {
          console.log("uploaded~");
          await projectSchema
            .findOneAndUpdate(
              {
                _id: project._id,
                "files.media.name": name,
              },
              {
                $set: {
                  "files.media.$.url": result.secure_url,
                },
              },
              { new: true }
            )
            .exec()
            .then(async (result) => {
              if (result) {
                await unlink(newLocation);
                await unlink(originalAudio);
                console.log(result);
                res.status(200).json(result);
              } else {
                success = false;
                res.status(400).json({
                  status: "failed",
                  message: "Updated Failed",
                });
              }
            });
        }
      });

    //upload the updated transcript file
    await cloudinaryStorage.uploader
      .upload(transcriptPath, {
        public_id: `${project.projectName}-transcript-file`,
        resource_type: "auto",
        use_filename: true,
        folder: "Videos/Transcriptions/Transcription-Files",
        overwrite: true,
      })
      .then(async (result) => {
        if (result) {
          await projectSchema
            .findOneAndUpdate(
              {
                _id: project._id,
                "files.transcription.name": fileName,
              },
              {
                $set: { "files.transcription.$.url": result.secure_url },
              },
              { new: true }
            )
            .exec()
            .then(async (result) => {
              if (result) {
                await unlink(transcriptPath);

                console.log(`${project.projectName} transcript file updated`);
              }
            });
        }
      });
  } catch (error) {
    console.log(error);
  }
};

//Insert New Audio and Word
export const addNewAudio = async (req, res) => {
  const { projectId } = req.params;
  const { start, end, newWord, gender, index } = req.body;
  let name, fileName, jsonData, oldDuration, newEnd, offset;
  try {
    req.setTimeout(6000 * 1000);

    const project = await projectSchema.findOne({ _id: projectId }).exec();
    project.files.media.map((media) => {
      name = media.name;
    });
    project.files.transcription.map((file) => {
      fileName = file.name;
    });

    const duration = getMp3Duration(req.files.newAudio[0].buffer);

    oldDuration = end - start;
    newEnd = start + duration;
    offset = Math.abs(oldDuration - newEnd);
    console.log(offset);

    const projectName = project.projectName.split(" ").join("");

    const localAudioPath = `${__dirname}/public/editing/new-audio-word/${newWord}.mp3`;

    const originalAudio = `${__dirname}/public/Original Audio/${projectName}-original.mp3`;

    const transcriptPath = `${__dirname}/public/temp/${projectName}-transcript-file.json`;

    // const newPath = `${__dirname}/public/editing/new-output/${projectName}-final-output.mp3`;

    console.log("start: ", start);
    console.log("end: ", end);
    console.log(req.files.newAudio);

    fs.writeFileSync(
      transcriptPath,
      Buffer.from(new Uint8Array(req.files.transcriptData[0].buffer))
    );
    console.log(index);
    var jsonFile = fs.readFileSync(transcriptPath, "utf8");
    jsonData = JSON.parse(jsonFile);

    console.log("current value: ", jsonData[0].elements[index].value);

    jsonData[0].elements[index].value = ` ${newWord} `;
    jsonData[0].elements[index].ts = start;
    jsonData[0].elements[index].type = "text";
    jsonData[0].elements[index].end_ts = newEnd;
    jsonData[0].elements[index].confidence = 1;

    for (var i = index; i <= jsonData[0].elements[0].length; i++) {
      if (jsonData[0].elements[i + 1].hasObjectProperty("ts")) {
        jsonData[0].elements[i + 1].ts += newEnd;
        jsonData[0].elements[i + 1].end_ts += newEnd;
      } else {
        jsonData[0].elements[i + 2].ts += newEnd;
        jsonData[0].elements[i + 2].end_ts += newEnd;
      }
    }

    console.log("new value : ", jsonData[0].elements[index].value);
    console.log("new ts : ", jsonData[0].elements[index].end_ts);
    console.log("new end_ts : ", jsonData[0].elements[index].ts);
    console.log("new confidence : ", jsonData[0].elements[index].confidence);

    await writeFile(transcriptPath, JSON.stringify(jsonData));

    console.log(`${transcriptPath} is updated`);

    fs.writeFileSync(
      localAudioPath,
      Buffer.from(new Uint8Array(req.files.newAudio[0].buffer))
    );
    console.log(`${localAudioPath} Created`);

    console.log(formatTime(duration));

    fs.writeFileSync(
      originalAudio,
      Buffer.from(new Uint8Array(req.files.original[0].buffer))
    );

    const output = await addNewAudioCut(
      originalAudio,
      localAudioPath,
      newWord,
      start,
      duration,
      projectName
    );

    console.log(`output - ${output}`);
    // const extName = path.extname(newPath).toString();
    // const file64 = parser.format(extName, req.file.buffer);

    await cloudinaryStorage.uploader
      .upload(output, {
        public_id: `${project.projectName}-${uniqueID()}-updated-new`,
        resource_type: "auto",
        use_filename: true,
        folder: "Videos/Editing/New",
      })
      .then(async (result) => {
        if (result) {
          console.log("uploaded~");
          await projectSchema
            .findOneAndUpdate(
              { _id: project._id, "files.media.name": name },
              {
                $set: {
                  "files.media.$.url": result.secure_url,
                },
              },
              { new: true }
            )
            .exec()
            .then(async (result) => {
              if (result) {
                // await unlink(newPath);
                await unlink(localAudioPath);
                await unlink(originalAudio);

                res.status(200).json(result);
              } else {
                success = false;
                res.status(400).json({
                  status: "failed",
                  message: "Updated Failed",
                });
              }
            });
        }
      });

    //upload the updated transcript file
    await cloudinaryStorage.uploader
      .upload(transcriptPath, {
        public_id: `${project.projectName}-transcript-file`,
        resource_type: "auto",
        use_filename: true,
        folder: "Videos/Transcriptions/Transcription-Files",
        overwrite: true,
      })
      .then(async (result) => {
        if (result) {
          await projectSchema
            .findOneAndUpdate(
              { _id: project._id, "files.transcription.name": fileName },
              {
                $set: { "files.transcription.$.url": result.secure_url },
              },
              { new: true }
            )
            .exec()
            .then(async (result) => {
              if (result) {
                await unlink(transcriptPath);

                console.log(`${project.projectName} transcript file updated`);
              }
            });
        }
      });
  } catch (error) {
    console.log(error);
  }
};

//Undo and Redo Changes
export const undo_redo_changes = async (req, res) => {
  const { word, start, end, confidence, index, operation } = req.body;
  const { projectId } = req.params;
  let fileName, mediaName, jsonData;

  try {
    console.log(req.files);
    const project = await projectSchema.findById(projectId).exec();

    project.files.media.map((media) => {
      mediaName = media.name;
    });
    project.files.transcription.map((file) => {
      fileName = file.name;
    });

    const projectName = project.projectName.split(" ").join("");

    const transcriptPath = `${__dirname}/public/temp/${projectName}-transcript-file.json`;

    const originalAudio = `${__dirname}/public/Original Audio/${projectName}-original.webm`;

    fs.writeFileSync(
      transcriptPath,
      Buffer.from(new Uint8Array(req.files.transcriptData[0].buffer))
    );

    console.log(index);
    console.log(`operation - ${operation}`);
    var jsonFile = fs.readFileSync(transcriptPath, "utf8");
    jsonData = JSON.parse(jsonFile);
    console.log("current value: ", jsonData[0].elements[index].value);

    if (operation === "add") {
      // jsonData[0].elements[index].value = ` ${word} `;
      jsonData[0].elements.splice(index, 1);

      console.log(`${transcriptPath} is updated`);
    } else if (operation === "edit") {
      jsonData[0].elements[index].value = word;
      jsonData[0].elements[index].ts = start;
      jsonData[0].elements[index].end_ts = end;
      jsonData[0].elements[index].confidence = confidence;
    } else if (operation === "delete") {
      jsonData[0].elements[index].value = word;
      jsonData[0].elements[index].ts = start;
      jsonData[0].elements[index].end_ts = end;
      jsonData[0].elements[index].confidence = confidence;
    }

    await writeFile(transcriptPath, JSON.stringify(jsonData));

    console.log(`${transcriptPath} is updated`);

    //upload the updated transcript file
    await cloudinaryStorage.uploader
      .upload(transcriptPath, {
        public_id: `${project.projectName}-transcript-file`,
        resource_type: "auto",
        use_filename: true,
        folder: "Videos/Transcriptions/Transcription-Files",
        overwrite: true,
      })
      .then(async (result) => {
        if (result) {
          await projectSchema
            .findOneAndUpdate(
              { _id: project._id, "files.transcription.name": fileName },
              {
                $set: { "files.transcription.$.url": result.secure_url },
              },
              { new: true }
            )
            .exec()
            .then(async (result) => {
              if (result) {
                await unlink(transcriptPath);
                console.log(`${project.projectName} transcript file updated`);
              }
            });
        }
      });

    fs.writeFileSync(
      originalAudio,
      Buffer.from(new Uint8Array(req.files.audio[0].buffer))
    );

    await cloudinaryStorage.uploader.upload(
      originalAudio,
      {
        public_id: `${project.projectName}-transcript-file`,
        resource_type: "auto",
        use_filename: true,
        folder: "Videos/Undo/",
        overwrite: true,
      },
      async function (error, result) {
        if (error) throw error;
        if (result) {
          console.log("uploaded~");
          await projectSchema
            .findOneAndUpdate(
              { _id: project._id, "files.media.name": mediaName },
              {
                $set: {
                  "files.media.$.url": result.secure_url,
                },
              },
              { new: true }
            )
            .exec()
            .then(async (result) => {
              if (result) {
                res.status(200).json(result);
                await unlink(originalAudio);
              } else {
                res.status(400).json({
                  status: "failed",
                  message: "Updated Failed",
                });
              }
            });
        }
      }
    );
  } catch (error) {
    console.log(error);
  }
};
