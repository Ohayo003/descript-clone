import userModel from "../models/user-model.js";
import projectSchema from "../models/project-files-model.js";
import fs from "fs";
import path, { extname, resolve } from "path";
import cloudinaryStorage from "../cloudinaryConfig.js";
import DataURIParser from "datauri/parser.js";
import { isObject, promisify } from "util";
import { RevAiApiClient } from "revai-node-sdk";
import revai from "revai-node-sdk";
import textToSpeech from "@google-cloud/text-to-speech";
import speechToText from "@google-cloud/speech";

const parser = new DataURIParser();
const __dirname = path.resolve();
const writeFile = promisify(fs.writeFile);
const unlink = promisify(fs.unlink);
const mkdir = promisify(fs.mkdir);

const accessToken = process.env.ACCESS_TOKEN;
const client = new RevAiApiClient(accessToken);
const speechClient = new speechToText.SpeechClient({
  keyFile: process.env.GOOGLE_ACCOUNT_CREDENTIALS,
  projectId: "text-to-speech-329005",
});
const textSpeechClient = new textToSpeech.TextToSpeechClient({
  keyFile: process.env.GOOGLE_ACCOUNT_CREDENTIALS,
  projectId: "text-to-speech-329005",
});

let uniqueID = () => {
  let ID = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  };

  return `${ID() + ID()}-${ID()}-${ID()}-${ID()}-${ID() + ID()}`;
};

async function TempDir() {
  await mkdir(`${__dirname}/public/transcriptions/`, { recursive: true });
}
TempDir();

//Create Project File
export const create_project_folder = async (req, res) => {
  const { projectName, email } = req.body;
  try {
    const uploadProject = new projectSchema({
      projectName,
      files: {
        media: [],
      },
      owner: email,
    });
    const userData = await userModel.findOne({ email: email }).exec();
    const checkProject = await projectSchema
      .findOne({ owner: userData.email, projectName: projectName })
      .exec();
    if (userData) {
      if (!checkProject) {
        await uploadProject.save().then(async (uploaded) => {
          console.log(uploaded);
          if (uploaded) {
            const userAlreadyHasProject = await userModel
              .findOne(
                { email: email },
                { "projectFiles.projectName": uploaded.projectName }
              )
              .exec();
            console.log(userAlreadyHasProject);
            if (userAlreadyHasProject) {
              await userModel
                .findOneAndUpdate(
                  { email: email },
                  {
                    $push: {
                      projectFiles: {
                        projectName,
                        createdAt: new Date(),
                        projectId: uploaded._id,
                      },
                    },
                  }
                )
                .exec()
                .then(() => {
                  return res.status(201).json(uploaded);
                });
            }
          } else {
            return res.status(400).json({
              status: "error",
              message: "Failed to upload file",
            });
          }
        });
      } else if (checkProject) {
        console.log(checkProject);
        return res.status(404).json({
          status: "Error",
          message: "Project Already Exist",
        });
      }
    }
  } catch (error) {
    console.error(error);
  }
};

//Get project by Project Id
export const getProject_byId = async (req, res) => {
  const { projectID } = req.params;
  try {
    if (projectID) {
      await projectSchema
        .findOne({ _id: projectID })
        .exec()
        .then(async (result) => {
          if (result) {
            res.status(200).json(result);
          } else {
            // console.log(result);
            res.status(500).json({
              status: "error",
              message: "something went wrong while fetching data",
            });
          }
        });
    }
  } catch (error) {
    console.log(error);
  }
};

//Gets all the projects of the user
export const getUsers_project = async (req, res) => {
  const { userID } = req.params;
  let email;
  try {
    const user = await userModel.findOne({ _id: userID }).exec();
    // console.log(user.email);
    if (user) {
      email = user.email;
      const project = await projectSchema.find({ owner: email }).exec();
      // console.log(stream);
      // console.log(project);
      res.status(200).json(project);
    }
  } catch (error) {
    console.error(error);
  }
};

//TODO::Delete the video on the cloudinary Storage before
//deleting the file in the database
//Delete Project Files
export const delete_projects = async (req, res) => {
  const { projectID, userId } = req.params;
  const { projectName } = req.body;
  let fileURL;
  // const { userId } = req.body;
  try {
    const project = await projectSchema.findById({ _id: projectID }).exec();
    // console.log(project);
    if (project && project.files.media.length) {
      fileURL = project.files.media[0].url;
    } else {
      fileURL = null;
    }
    // await cloudinaryStorage.uploader.destroy(fileURL).then(async (result) => {
    //   if (result) {
    //     if (project.projectName === projectName) {
    //       res.status(400).json({
    //         status: "error",
    //         message: `cannot delete default folder ${projectName}`,
    //       });
    //     }
    await projectSchema
      .findByIdAndRemove({ _id: project._id })
      .exec()
      .then(async (result) => {
        if (result) {
          await userModel
            .findByIdAndUpdate(
              { _id: userId },
              {
                $pull: {
                  projectFiles: { projectId: project._id },
                },
              }
            )
            .exec()
            .then((result) => {
              if (result) {
                res.status(200).json({
                  status: "success",
                  message: "project removed!",
                });
              }
            });
        }
      });
    // }
    // });
  } catch (error) {
    console.log(error);
  }
};

//Update project file by uploading video or audio into the cloudinary storage
export const upload_file = async (req, res) => {
  const { projectID, userId } = req.params;
  let url;
  try {
    const userData = await userModel.findById(userId).exec();

    const projectData = await projectSchema.findById(projectID).exec();
    // Update users project with the file uploaded
    console.log("projectID: ", projectID, "userID: ", userId);
    if (userData.email === projectData.owner) {
      console.log(req.file);
      const fileType = req.file.mimetype;
      console.log("fileType: ", fileType);

      const extName = path.extname(req.file.originalname).toString();
      const file64 = parser.format(extName, req.file.buffer);

      const cloud = await cloudinaryStorage.uploader.upload(file64.content, {
        public_id: req.file.originalname,
        resource_type: "auto",
        use_filename: true,
        folder: "Videos/Local Uploads/",
      });
      // .then(async (result) => {

      // })
      // .catch((error) => console.error(error));
      if (cloud) {
        console.log("file uploaded to storage");
        url = cloud.secure_url;
        // }
      }
      console.log(`original name - ${req.file.originalname}`);
      await projectSchema
        .findByIdAndUpdate(
          projectData._id,
          {
            $push: {
              "files.media": {
                name: req.file.originalname,
                id: uniqueID(),
                url: url,
                createdAt: new Date(),
              },
            },
          },
          { new: true }
        )
        .exec()
        .then((result) => {
          if (result) {
            console.log(result);
            res.status(200).json(result);
          } else {
            res.status(400).json({
              status: "error",
              message: "Failed to update",
            });
          }
        });
    }
  } catch (error) {
    console.log(error);
  }
};

//transcript media file
export const transcript_media_file = async (req, res) => {
  const { projectID } = req.params;
  const { videoURL } = req.body;
  let jobDetails;

  try {
    const projectData = await projectSchema.findOne({ _id: projectID }).exec();

    const account = await client.getAccount();

    console.log(`Account: ${account.email}`);

    //Get the remaining time usable for transcription
    console.log(`Credits remaining: ${account.balance_seconds} seconds`);

    //Submit file using URL
    const job = await client.submitJobUrl(videoURL, {
      language: "en",
      skip_diarization: false,
    });
    // Get the Job Details of the submitted Job

    const localTransPath = `${__dirname}/public/transcriptions/${job.id}-${projectID}-transcription.json`;

    console.log("transcription is starting....");
    while (
      (jobDetails = (await client.getJobDetails(job.id)).status) ===
      revai.JobStatus.InProgress
    ) {
      console.log(`Job ${job.id} is ${jobDetails}`);
      await new Promise((resolve) => setTimeout(resolve, 5000));
    }
    console.log(`Job ${job.id} is now ${job.status}`);

    //Get the Transcript file object
    var transcriptObj = await client.getTranscriptObject(job.id);

    await writeFile(
      localTransPath,
      JSON.stringify(transcriptObj.monologues, null, 2)
    );

    // Upload transcription file into cl oudinary storage
    await cloudinaryStorage.uploader.upload(
      localTransPath,
      {
        public_id: `${projectData.projectName}-transcript-file`,
        resource_type: "auto",
        use_filename: true,
        folder: "Videos/Transcriptions/Transcription-Files",
      },
      async function (error, result) {
        if (error) throw error;
        if (result) {
          await projectSchema
            .findByIdAndUpdate(
              projectData._id,
              {
                $push: {
                  "files.transcription": {
                    name: `${projectID}-${job.id}-transcript`,
                    url: result.secure_url,
                    createdAt: new Date(),
                  },
                },
                $set: {
                  transcribed: true,
                },
              },
              { new: true, upsert: true }
            )
            .then(async (result) => {
              if (result) {
                console.log(result);
                await unlink(localTransPath);
                console.log("local file removed");
                res.status(200).json(result);
              } else {
                res.status(400).json({
                  status: "error",
                  message: "Failed to update data",
                });
              }
            });
        }
      }
    );
  } catch (error) {
    console.log(error);
  }
};

//Get the transcript File
export const get_transcript = async (req, res) => {
  res.status(200).json({ message: "Transcript file" });
};

//Upload video recordings to the cloudinary storage
export const upload_video_recordings = async (req, res) => {
  const { userId } = req.params;
  const { projectName } = req.body;
  let project_id;
  try {
    req.setTimeout(6000 * 1000);
    await mkdir(`${__dirname}/public/recordings/`, { recursive: true });
    const userProject = await userModel.findOne({ _id: userId }).exec();

    userProject.projectFiles.map((project) => {
      if (project.projectName === projectName) project_id = project.projectId;
    });
    console.log(req.file);
    const recordingFile = `${__dirname}/public/recordings/${userProject._id}-Recording.mp4`;
    fs.writeFileSync(
      recordingFile,
      Buffer.from(new Uint8Array(req.file.buffer))
    );

    // Upload the recorded video into the Recordins file
    const extName = path.extname(recordingFile).toString();
    console.log(extName);
    await cloudinaryStorage.uploader
      .upload(recordingFile, {
        public_id: `${userProject._id}-Recording`,
        resource_type: "auto",
        use_filename: true,
        folder: "Videos/Recordings",
      })
      .then(async (result) => {
        try {
          if (result) {
            console.log("file uploaded to storage");
            await projectSchema
              .findByIdAndUpdate(project_id, {
                $push: {
                  "files.media": {
                    name: `${userProject._id}-Recording`,
                    id: uniqueID(),
                    url: result.secure_url,
                    createdAt: new Date(),
                  },
                },
              })
              .exec()
              .then(async (result) => {
                if (result) {
                  await unlink(recordingFile);
                  return res.status(200).json({
                    status: "upload successful",
                    message: result,
                  });
                }
              });
          }
        } catch (error) {
          console.log(error);
        }
      });
  } catch (error) {
    console.log(error);
  }
};

//Upload video recordings to the cloudinary storage
export const upload_podcasting_recordings = async (req, res) => {
  const { userId } = req.params;
  const { projectName } = req.body;
  let project_id;

  try {
    req.setTimeout(6000 * 1000);
    await mkdir(`${__dirname}/public/podcasting/`, { recursive: true });
    const userProject = await userModel.findOne({ _id: userId }).exec();

    userProject.projectFiles.map((project) => {
      if (project.projectName === projectName) project_id = project.projectId;
    });

    const recordingFile = `${__dirname}/public/podcasting/${userProject._id}-Podcast-Recording.mp3`;
    console.log(req.file);
    // await writeFile(recordingFile, videoUrl);

    fs.writeFileSync(
      recordingFile,
      Buffer.from(new Uint8Array(req.file.buffer))
    );

    // const extName = path.extname(recordingFile).toString();
    // console.log(extName);
    // const file64 = parser.format(extName, req.file.buffer);
    await cloudinaryStorage.uploader.upload(
      recordingFile,
      {
        public_id: `${userProject._id}-Podcast-Recording`,
        resource_type: "video",
        use_filename: true,
        folder: "Videos/Recordings/Podcasting",
      },
      async function (error, result) {
        if (error) throw error;
        if (result) {
          projectSchema
            .findByIdAndUpdate(
              project_id,
              {
                $push: {
                  "files.media": {
                    name: `${userProject._id}-Podcast-Recording`,
                    id: project_id,
                    url: result.secure_url,
                    createdAt: new Date(),
                  },
                },
              },
              { new: true, upsert: true }
            )
            .exec()
            .then(async (result) => {
              console.log("file uploaded to storage");
              await unlink(recordingFile);
              res.status(200).send(result);
            });
        }
      }
    );
  } catch (error) {
    console.log(error);
  }
};

//Get All Video Recordings
export const get_video_recordings = async (req, res) => {
  const { userId } = req.params;
  let recordingId;
  try {
    if (userId) {
      const user = await userModel.findOne({ _id: userId }).exec();
      if (user.projectFiles) {
        user.projectFiles.map((project) => {
          if (project.projectName === "Recordings") {
            recordingId = project.projectId;
            console.log(project);
          }
        });
      }
      await projectSchema
        .findOne({ _id: recordingId })
        .exec()
        .then((result) => {
          if (result) {
            console.log(result);
            return res.status(200).json(result);
          }
        });
    }
  } catch (error) {
    console.log(error);
  }
};

//Upload existing recording on the workspace of
export const use_media_from_workspace = async (req, res) => {
  const { userId } = req.params;
  const { recordingId, projectId } = req.body;
  let record_data;
  let projectID;
  let data;
  try {
    const user_project = await userModel.findOne({ _id: userId }).exec();
    const currentProject = await projectSchema
      .findOne({ _id: projectId })
      .exec();

    user_project.projectFiles.map((project) => {
      if (project.projectName === "Recordings") {
        record_data = project;
        projectID = project.projectId;
      }
    });

    const recordings = await projectSchema.findOne({
      _id: record_data.projectId,
    });

    if (recordings.owner === user_project.email) {
      recordings.files.media.map((record) => {
        if (record.id === recordingId) {
          data = record;
        }
      });
    }

    await projectSchema
      .findByIdAndUpdate(currentProject._id, {
        $push: {
          "files.media": {
            name: data.name,
            id: uniqueID(),
            url: data.url,
            createdAt: new Date(),
          },
        },
      })
      .exec()
      .then((result) => {
        if (result) {
          res.status(200).json(result);
        } else {
          res.status(400).json({
            status: "error",
            message: "Failed to update",
          });
        }
      });
  } catch (error) {
    console.log(error);
  }
};
