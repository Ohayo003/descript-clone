import AudioSchema from "../models/existing-audio-model.js";
import userModel from "../models/user-model.js";
import fs from "fs";
import DataURIParser from "datauri/parser.js";
import path from "path";
import cloudinaryStorage from "../cloudinaryConfig.js";
import { promisify } from "util";

const parser = new DataURIParser();

const __dirname = path.resolve();

const writeFile = promisify(fs.writeFile);

const unlink = promisify(fs.unlink);
const mkdir = promisify(fs.mkdir);

const createDirectory = async () => {
  await mkdir(`${__dirname}/public/temp/audio/`, { recursive: true });
};

createDirectory();

let uniqueID = () => {
  let ID = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  };

  return `${ID() + ID()}-${ID()}-${ID() + ID()}`;
};

//Create folder on sign up
export const create_trained_folder = async (req, res) => {
  const { projectName, email } = req.body;
  try {
    let createAudio = {
      projectName: "Trained_Audio",
      audioFiles: [],
      owner: email,
    };
    const project = new AudioSchema(createAudio);

    await project.save().then(async (result) => {
      if (result) {
        await userModel
          .updateOne(
            { email: email },
            {
              $push: {
                projectFiles: {
                  projectName,
                  createdAt: new Date(),
                  projectId: result._id,
                },
              },
            }
          )
          .exec()
          .then(async (result) => {
            if (result) {
              return res.status(201).json(result);
            }
          });
      }
    });
  } catch (error) {
    console.log(error);
  }
};

//Train new Audio
export const train_new_audio = async (req, res) => {
  const { name } = req.body;
  const { userId } = req.params;
  let fileName, email, id;
  try {
    console.log(req.file);
    console.log(name);
    const userProject = await userModel.findById(userId).exec();
    email = userProject.email;
    userProject.projectFiles.map((files) => {
      console.log(files);
      if (files.projectName === "Trained_Audio") {
        id = files.projectId;
      }
    });
    fileName = name.toLowerCase();
    const audioName = name.split(" ").join("");
    console.log(id);
    const trained_audio = await AudioSchema.findById(id).exec();

    const newAudio = `${__dirname}/public/temp/audio/${audioName}.mp3`;

    fs.writeFileSync(newAudio, Buffer.from(new Uint8Array(req.file.buffer)));

    if (email === trained_audio.owner) {
      await cloudinaryStorage.uploader
        .upload(newAudio, {
          public_id: `${fileName}`,
          resource_type: "auto",
          use_filename: true,
          folder: "Video/Audio-Tranings",
        })
        .then(async (result) => {
          console.log("uploaded");
          if (result) {
            await AudioSchema.findByIdAndUpdate(
              { _id: trained_audio._id },
              {
                $push: {
                  audioFile: {
                    name: `${name}`,
                    id: uniqueID(),
                    url: result.secure_url,
                    createAt: new Date(),
                  },
                },
              },
              { new: true }
            )
              .exec()
              .then(async (result) => {
                if (result) {
                  await unlink(newAudio);
                  return res.status(201).json(result);
                }
              });
          }
        });
    }

    // const extName = path.extname(newAudio).toString();
    // const file64 = parser.
  } catch (error) {
    console.log(error);
  }
};

//Get existing audio by name
export const get_existing_audio_by_name = async (req, res) => {
  const { userId } = req.params;
  const { audioName } = req.body;
  let email, name;

  try {
    const userProject = await userModel.findOne({ _id: userId }).exec();

    email = userProject.email;
    const audioProject = await AudioSchema.findOne({ owner: email });
    // userProject.projectFiles.map((files) => {
    //   if (files.projectName === "Trained_Audio") {
    //     trained_audio_id = files.projectId;
    //   }
    // });
    if (audioName) {
      name = audioName.toLowerCase();
    }
    console.log("audio name: ", name);

    await AudioSchema.findOne(
      {
        _id: audioProject._id,
      },

      {
        audioFile: {
          $elemMatch: {
            name: name,
          },
        },
      }
    )
      .exec()
      .then(async (result, error) => {
        if (error) {
          return res.status(404).json({
            status: "error",
            message: "file does not exist",
          });
        }
        if (result) {
          return res.status(200).json(result);
        } else {
          return res.status(404).json({
            status: "error",
            message: "file does not exist",
          });
        }
      });
  } catch (error) {
    console.log(error);
  }
};

//Get Existing audio
export const get_audio_sounds = async (req, res) => {
  const { userId } = req.params;
  let email, trained_audio_id;

  try {
    const userProject = await userModel.findOne({ _id: userId }).exec();
    userProject.projectFiles.map((files) => {
      if (files.projectName === "Trained_Audio") {
        trained_audio_id = files.projectId;
      }
    });
    email = userProject.email;

    const audioProject = await AudioSchema.findById({
      _id: trained_audio_id,
    }).exec();

    if (email === audioProject.owner) {
      console.log(trained_audio_id);
      await AudioSchema.find({ _id: trained_audio_id })
        .exec()
        .then(async (result) => {
          if (result) {
            console.log("result: ", result);
            return res.status(200).json(result);
          } else {
            return res.status(404).json({
              status: "error",
              message: "file does not exist",
            });
          }
        });
    }
  } catch (error) {
    console.log(error);
  }
};

export const delete_existing_audio = async (req, res) => {
  const { audioId, audioName } = req.body;
  const { userId } = req.params;

  try {
    const userProject = await userModel.findById(userId).exec();
    const email = userProject.email;
    const audioProjects = await AudioSchema.findOne({ owner: email }).exec();

    if (audioId) {
      await AudioSchema.updateOne(
        { _id: audioProjects._id },
        {
          $pull: {
            audioFile: { id: audioId },
          },
        },
        { multi: true, new: true }
      )
        .exec()
        .then(async (result) => {
          if (result) {
            res.status(200).json({
              status: "success",
              message: "audio deleted.",
            });
          } else {
            res.status(400).json({
              status: "error",
              message: "failed to delete audio",
            });
          }
        });
    }
  } catch (error) {
    console.log(error);
  }
};
