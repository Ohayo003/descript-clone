import express from "express";
import cors from "cors";
import mongoose from "mongoose";
import dotenv from "dotenv";
import route from "./routes/user-routes.js";
import path from "path";
// import helmet from "helmet";
import projects from "./routes/projects-routes.js";
import edit from "./routes/edit-routes.js";
import audio from "./routes/audio-routes.js";

dotenv.config();

const app = express();
const PORT = process.env.PORT || 5000;
const __dirname = path.resolve();

//Middlewares
// app.use(helmet());
// app.use(
//   cors({
//     origin: "*",
//     methods: ["POST", "PUT", "DELETE", "GET"],
//   })
// );
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.set("trust proxy", 1);

// Configurations for "Static-files"
app.set("view engine", "ejs");

// DBConfig
mongoose.connect(process.env.DB_URI, (error) => {
  if (error) {
    console.log("Failed to Connect to Database");
    console.log(error);
    process.exit(0);
  } else {
    console.log("Connected to the Database!");
  }
});

// Endpoint
app.use("/server/v1/users", route);
app.use("/server/v1/projects", projects);
app.use("/server/v1/train-audio", audio);
app.use("/server/v1/projects/edit", edit);

if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.join(__dirname, "/client/build")));

  app.get("/*", (_, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
} else {
  app.get("/", (_, res) => {
    res.send("Welcome to express");
  });
}

app.listen(PORT, () => {
  console.log(`Server is up and running on PORT: ${PORT}`);
});
