import cloudinary from "cloudinary";
import dotenv from "dotenv";

dotenv.config();

const cloudinaryStorage = cloudinary.v2;

cloudinaryStorage.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET,
});

export default cloudinaryStorage;
