import mongoose from "mongoose";

const projectSchema = new mongoose.Schema(
  {
    projectName: {
      type: String,
      required: true,
    },
    files: {
      transcription: { type: Object },
      media: { type: Array },
    },
    owner: {
      type: String,
      required: true,
    },
    transcribed: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

export default mongoose.model("projects", projectSchema);
