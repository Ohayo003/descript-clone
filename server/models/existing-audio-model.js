import mongoose from "mongoose";

const AudioSchema = new mongoose.Schema(
  {
    projectName: {
      type: String,
    },
    audioFile: [],
    owner: {
      type: String,
    },
  },
  { timestamps: true }
);

export default mongoose.model("Audio", AudioSchema);
