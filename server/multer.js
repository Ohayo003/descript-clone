import multer from "multer";

// // Configuration for Multer
// const multerStorage = multer.diskStorage({
//   destination: async (req, file, cb) => {
//     cb(null, "public");
//   },
//   filename: (req, file, cb) => {
//     cb(null, `/${file.originalname}`);
//   },
// });
// Multer Filter
const multerFilter = (req, file, cb) => {
  if (
    //     file.mimetype.split("/")[1] === "webm" ||
    //     file.mimetype.split("/")[1] === "mkv" ||
    //     file.mimetype.split("/")[1] === "mp4" ||
    file.mimetype.split("/")[1] === "video" ||
    //     file.mimetype.split("/")[1] === "mp3" ||
    //     file.mimetype.split("/")[1] === "mpeg" ||
    file.mimetype.split("/")[1] === "audio"
  ) {
    cb(null, true);
  } else {
    cb(new Error("File is not an audio or video!!"), false);
  }
};

// Calling the "multer" Function
export const upload = multer({
  storage: multer.memoryStorage(),
  // fileFilter: multerFilter,
});
