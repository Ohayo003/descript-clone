import filePath from "path";
import fs from "fs";
import { createFFmpeg, fetchFile } from "@ffmpeg/ffmpeg";
import { fileURLToPath } from "url";
import { promisify } from "util";
// import MP3Cutter from "mp3-cutter";

const __dirname = filePath.resolve();
const __filename = fileURLToPath(import.meta.url);
const mkdir = promisify(fs.mkdir);
const writeFile = promisify(fs.writeFile);

export function formatTime(seconds = 0) {
  const minutes = Math.floor((seconds % 3600) / 60)
    .toString()
    .padStart(2, "0");
  const newSeconds = (seconds % 60).toFixed(3).toString().padStart(6, "0");
  const hours = Math.floor(seconds / 3600)
    .toString()
    .padStart(2, "0");
  return `${hours || "00"}:${minutes || "00"}:${
    newSeconds || seconds.toString().padStart(6, "0")
  }`;
}

export async function extractAudioFromVideo(
  path,
  start,
  end,
  duration,
  originWordStart,
  originWordEnd,
  project
) {
  try {
    const fileOutput = `${__dirname}/public/edit/${projectName}-Original.mp4`;
    const ffmpeg = createFFmpeg({ log: true });

    await ffmpeg.load();

    ffmpeg.FS("writeFile", `${projectName}-Audio.mp3`, await fetchFile(path));

    await ffmpeg.run(
      "-i",
      `${projectName}-Audio.mp3`,
      "-ss",
      `${formatTime(originWordStart)}`,
      "-to",
      `${formatTime(originWordEnd)}`,
      `${projectName}-Word.mp3`
    );
    await ffmpeg.run(
      "-ss",
      `00:00:00`,
      "-i",
      `${projectName}-Audio.mp3`,
      "-t",
      formatTime(start),
      `${projectName}-1.mp3`
    );
    await ffmpeg.run(
      "-ss",
      formatTime(end),
      "-i",
      `${projectName}-Audio.mp3`,
      `${projectName}-2.mp3`
    );
    await ffmpeg.run(
      "-i",
      `${projectName}-1.mp3`,
      "-i",
      `${projectName}-Word.mp3`,
      "-i",
      `${projectName}-2.mp3`,
      "-filter_complex",
      "[0:1][1:a][2:a]concat=n=3:v=0:a=1",
      `${projectName}-Original.mp4`
    );

    await fs.promises.writeFile(
      fileOutput,
      ffmpeg.FS("readFile", `${projectName}-Original.mp4`)
    );

    ffmpeg.FS("unlink", `${projectName}-Orignal.mp4`);
    ffmpeg.FS("unlink", `${projectName}-1.mp3`);
    ffmpeg.FS("unlink", `${projectName}-2.mp3`);
    ffmpeg.FS("unlink", `${projectName}-Word.mp3`);

    return fileOutput;
  } catch (error) {
    console.log(error);
  }
}

//Add bew audio to the original audio
export async function addNewAudioCut(
  original,
  newAudio,
  newWord,
  start,
  duration,
  projectName
) {
  const ffmpeg = createFFmpeg({ log: true });
  var end;
  try {
    end = start + duration;
    const fileOutput = `${__dirname}/public/editing/new-output/${projectName}-final-output.mp3`;

    await ffmpeg.load();

    ffmpeg.FS(
      "writeFile",
      `${projectName}-original-audio.mp3`,
      await fetchFile(original)
    );

    ffmpeg.FS("writeFile", `${newWord}-audio.mp3`, await fetchFile(newAudio));

    await ffmpeg.run(
      "-i",
      `${newWord}-audio.mp3`,
      "-ss",
      formatTime(start),
      "-to",
      formatTime(duration),
      `${newWord}-Word.mp3`
    );

    await ffmpeg.run(
      "-ss",
      "00:00:00",
      "-i",
      `${projectName}-original-audio.mp3`,
      "-t",
      formatTime(start),
      `${projectName}-1.mp3`
    );

    await ffmpeg.run(
      "-ss",
      formatTime(end),
      "-i",
      `${projectName}-original-audio.mp3`,
      `${projectName}-2.mp3`
    );

    console.log(`new end  - ${formatTime(start) + formatTime(duration)}`);

    await ffmpeg.run(
      "-i",
      `${projectName}-1.mp3`,
      "-i",
      `${newWord}-audio.mp3`,
      "-i",
      `${projectName}-2.mp3`,
      "-filter_complex",
      "[0:0][1:0][2:0]concat=n=3:v=0:a=1[out]",
      "-map",
      "[out]",
      `${projectName}-final-output.mp3`
    );
    console.log(`new end: ${formatTime(end)}`);

    console.log(`path - ${fileOutput}`);

    const data = ffmpeg.FS("readFile", `${projectName}-final-output.mp3`);

    console.log(data);

    await fs.promises.writeFile(fileOutput, data);

    console.log(`ouput - ${fileOutput}`);

    ffmpeg.FS("unlink", `${projectName}-final-output.mp3`);
    ffmpeg.FS("unlink", `${newWord}-audio.mp3`);
    ffmpeg.FS("unlink", `${projectName}-1.mp3`);
    ffmpeg.FS("unlink", `${projectName}-2.mp3`);

    return fileOutput;
  } catch (error) {
    console.log(error);
  }
}

export async function audioCut(
  start,
  end,
  projectName,
  fileOutput,
  path,
  newPath
) {
  const ffmpeg = createFFmpeg({ log: true });

  // const fileOutput = `${__dirname}/public/edit/${projectName}-Output.mp4`;
  try {
    // fs.promises.mkdir(
    //   `${__dirname}/public/editing/edited/new-${projectName}-output/`,
    //   { recursive: true }
    // );

    await ffmpeg.load();

    ffmpeg.FS("writeFile", `${projectName}.mp3`, await fetchFile(path));

    ffmpeg.FS(
      "writeFile",
      `${projectName}-Edited.mp3`,
      await fetchFile(newPath)
    );

    await ffmpeg.run(
      "-ss",
      `00:00:00`,
      "-i",
      `${projectName}.mp3`,
      "-t",
      formatTime(start),
      `${projectName}-1.mp3`
    );
    await ffmpeg.run(
      "-ss",
      formatTime(end),
      "-i",
      `${projectName}.mp3`,
      `${projectName}-2.mp3`
    );
    await ffmpeg.run(
      "-i",
      `${projectName}-1.mp3`,
      "-i",
      `${projectName}-Edited.mp3`,
      "-i",
      `${projectName}-2.mp3`,
      "-filter_complex",
      "[0:0][1:0][2:0]concat=n=3:v=0:a=1[out]",
      "-map",
      "[out]",
      `${projectName}-Final-Output.mp3`
    );
    const data = ffmpeg.FS("readFile", `${projectName}-Final-Output.mp3`);
    await writeFile(fileOutput, data);

    ffmpeg.FS("unlink", `${projectName}-Final-Output.mp3`);
    ffmpeg.FS("unlink", `${projectName}-1.mp3`);
    ffmpeg.FS("unlink", `${projectName}-2.mp3`);
    ffmpeg.FS("unlink", `${projectName}-Edited.mp3`);

    return fileOutput;
  } catch (error) {
    console.log(error);
  }
}

export async function ReplaceExisting(
  path,
  newAudioPath,
  newWord,
  start,
  duration,
  end,
  projectName
) {
  const ffmpeg = createFFmpeg({ log: true });

  const fileOutput = `${__dirname}/public/editing/new-output/${projectName}-final-output.mp3`;

  try {
    // fs.promises.mkdir(
    //   `${__dirname}/public/editing/edited/new-${projectName}-output/`,
    //   { recursive: true }
    // );
    var newEnd = start + duration;
    await ffmpeg.load();

    ffmpeg.FS(
      "writeFile",
      `${projectName}-original-audio.mp3`,
      await fetchFile(path)
    );

    ffmpeg.FS(
      "writeFile",
      `${newWord}-audio.mp3`,
      await fetchFile(newAudioPath)
    );

    // await ffmpeg.run(
    //   "-i",
    //   `${newWord}-audio.mp3`,
    //   "-ss",
    //   formatTime(start),
    //   "-to",
    //   formatTime(duration),
    //   `${newWord}-Word.mp3`
    // );
    await ffmpeg.run(
      "-ss",
      "00:00:00",
      "-i",
      `${projectName}-original-audio.mp3`,
      "-t",
      formatTime(start),
      `${projectName}-1.mp3`
    );
    await ffmpeg.run(
      "-ss",
      formatTime(end),
      "-i",
      `${projectName}-original-audio.mp3`,
      `${projectName}-2.mp3`
    );

    // console.log("Ine 303 : ", `${newPath}`);
    await ffmpeg.run(
      "-i",
      `${projectName}-1.mp3`,
      "-i",
      `${newWord}-audio.mp3`,
      "-i",
      `${projectName}-2.mp3`,
      "-filter_complex",
      "[0:a][1:a][2:a]concat=n=3:v=0:a=1",
      `${projectName}-final-output.mp3`
    );

    const data = ffmpeg.FS("readFile", `${projectName}-final-output.mp3`);
    // console.log(`new Path - ${data}`);

    await fs.promises.writeFile(fileOutput, data);

    console.log(`new Path 311 - ${fileOutput}`);

    ffmpeg.FS("unlink", `${projectName}-final-output.mp3`);
    // ffmpeg.FS("unlink", `${newWord}-Word.mp3`);
    ffmpeg.FS("unlink", `${newWord}-audio.mp3`);
    ffmpeg.FS("unlink", `${projectName}-1.mp3`);
    ffmpeg.FS("unlink", `${projectName}-2.mp3`);

    return fileOutput;
  } catch (error) {
    console.log(error);
  }
}

export async function removeAudio(path, start, end, projectName) {
  const ffmpeg = createFFmpeg({ log: true });
  const fileOutput = `${__dirname}/public/editing/cut-audio/${projectName}-cutted-output.mp3`;

  try {
    await ffmpeg.load();

    ffmpeg.FS("writeFile", `${projectName}.mp3`, await fetchFile(path));

    await ffmpeg.run(
      "-ss",
      `00:00:00`,
      "-i",
      `${projectName}.mp3`,
      "-t",
      formatTime(start),
      `${projectName}-1.mp3`
    );
    await ffmpeg.run(
      "-ss",
      formatTime(end),
      "-i",
      `${projectName}.mp3`,
      `${projectName}-2.mp3`
    );
    await ffmpeg.run(
      "-i",
      `${projectName}-1.mp3`,
      "-i",
      `${projectName}-2.mp3`,
      "-filter_complex",
      "[0:0][1:0]concat=n=2:v=0:a=1[out]",
      "-map",
      "[out]",
      `${projectName}-cutted-output.mp3`
    );

    const data = ffmpeg.FS("readFile", `${projectName}-cutted-output.mp3`);

    await fs.promises.writeFile(fileOutput, data);

    ffmpeg.FS("unlink", `${projectName}-cutted-output.mp3`);
    // ffmpeg.FS("unlink", `${projectName}-Edited.mp3`);
    ffmpeg.FS("unlink", `${projectName}-2.mp3`);
    ffmpeg.FS("unlink", `${projectName}-1.mp3`);

    return fileOutput;
  } catch (error) {
    console.log(error);
  }
}
