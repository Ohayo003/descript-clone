import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { Card } from "@material-ui/core";
import Typography from "@mui/material/Typography";
import { Box } from "@mui/system";
import { IconButton } from "@material-ui/core";
import Button from "@material-ui/core/Button";
// import PlayArrowIcon from "@material-ui/icons/PlayArrow";
// import StopIcon from "@material-ui/icons/Stop";
import DoneIcon from "@material-ui/icons/Done";
import VolumeUpIcon from "@mui/icons-material/VolumeUp";
import DeleteIcon from "@mui/icons-material/Delete";
import "./AvailableSoundsModal.css";
import axios from "../../axios";

function AvailableSoundsModal({
  show,
  setShowAvailableSounds,
  user,
  setSounds,
  setAudioFromModal,
}) {
  const [availableSounds, setAvailableSounds] = useState(null);

  const handleHideModal = () => setShowAvailableSounds(false);
  const [isDelete, setIsDelete] = useState(false);
  const [audioId, setAudioId] = useState("");

  const handleClickAudio = (audio) => {
    setAudioFromModal(true);
    setSounds({ word: audio.name, url: audio.url });
    handleHideModal();
  };

  const handleDelete = (audio) => {
    if (audio) {
      setAudioId(audio.id);
      console.log(audioId);
      console.log(isDelete);
    } else {
      setAudioId("");
      setIsDelete(false);
    }
  };

  useEffect(() => {
    try {
      if (user && show) {
        const getAvailableSounds = async () => {
          await axios
            .get(`/train-audio/get-audio-sounds/${user._id}`)
            .then(async (result) => {
              if (result) {
                console.log(result.data);
                setAvailableSounds(result.data[0]);
              }
            });
        };
        getAvailableSounds();
        // console.log(availableSounds.audioFiles);
      }
    } catch (error) {
      console.log(error);
    }
  }, [show, user]);

  useEffect(() => {
    if (isDelete) {
      console.log(`delete : ${isDelete}`);
      try {
        const deleteAudio = async () => {
          let data = {
            audioId: audioId,
          };
          await axios
            .delete(`/train-audio/delete-audio/${user._id}`, data)
            .then(() => {
              setIsDelete(false);
              setAudioId("");
            });
        };
        deleteAudio();
      } catch (error) {
        console.log(error);
      }
    }
  }, [audioId, isDelete, user._id]);

  return (
    <Modal
      scrollable={true}
      show={show}
      centered
      size="sm"
      onHide={handleHideModal}
      animation={true}
    >
      <Modal.Header closeButton>
        <Typography variant="h6">Available Sounds</Typography>
      </Modal.Header>
      <Modal.Body>
        {availableSounds && availableSounds.audioFile.length ? (
          availableSounds.audioFile.map((audio, index) => (
            <Card className="audio-card-container" key={index}>
              <Box sx={{ display: "flex", flexDirection: "row" }}>
                <Box sx={{ p: 1, flexGrow: 1 }}>
                  <Typography variant="subtitle1">{audio.name}</Typography>
                </Box>
                <Box sx={{ display: "flex", flexDirection: "row", p: 1 }}>
                  <Box>
                    <VolumeUpIcon />
                  </Box>
                  <Box>
                    <IconButton
                      size="small"
                      color="secondary"
                      onClick={() => {
                        handleDelete(audio);
                        setIsDelete(true);
                      }}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </Box>
                  <Box>
                    <IconButton
                      size="small"
                      color="primary"
                      onMouseDown={() => handleClickAudio(audio)}
                    >
                      <DoneIcon />
                    </IconButton>
                  </Box>
                </Box>
              </Box>
            </Card>
          ))
        ) : (
          <Typography variant="subtitle1" style={{ color: "lightgray" }}>
            There are currently no trained audio
          </Typography>
        )}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="contained" color="secondary" onClick={handleHideModal}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default AvailableSoundsModal;
