import React, { useEffect, useRef, useState } from "react";
import { Modal, Form } from "react-bootstrap";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import { toast } from "react-toastify";
import { options } from "../../ToastifyOptions";
import RecordNewAudioForNewWord from "./RecordNewAudioForNewWord";
import axios from "../../axios";
import AvailableSoundsModal from "./AvailableSoundsModal";

function EditModal({
  showModify,
  setShowModify,
  selectWord,
  setSelectWord,
  setRedo,
  user,
  setUndo,
  setIsExistingAudio,
  isExistingAudio,
  setIsAdd,
  setNewAudioBlob,
  setDelete,
  setEdit,
  undo,
}) {
  const newWord = useRef();
  const [start, setStart] = useState(0);
  const [end, setEnd] = useState(0);
  const [checkEditOrAdd, setCheckEditOrAdd] = useState({
    edit: false,
    add: false,
  });
  const [showAvailableAudioSound, setShowAvailableAudioSound] = useState(false);
  const hideModal = () => setShowModify(false);
  const [audioFromModal, setAudioFromModal] = useState(false);
  const [showNewRecordModal, setShowNewRecordModal] = useState(false);
  const [sounds, setSounds] = useState({ word: "", url: "" });

  const deleteWord = () => {
    if (selectWord) {
      setUndo({ word: selectWord.word, position: selectWord.position });
      // setRedo({ word: undo.word, position: undo.position });
      setDelete(true);
      setSelectWord({
        word: "",
        position: selectWord.position,
        start: start,
        end: end,
      });

      hideModal();
    }
  };

  const checkIfAudioIsExisting = async (word) => {
    try {
      console.log("checkExist : ", word);
      const data = {
        audioName: word,
      };
      console.log("data word : ", data.audioName);
      const existingAudio = axios.get(
        `/train-audio/get-audio/${user._id}`,
        data
      );
      await toast
        .promise(
          existingAudio,
          {
            pending: "Checking audio availability. Please wait...",
            success: "Audio is available.",
            error: "Audio does not exist. Create an Audio",
          },
          options
        )
        .then(async (result) => {
          if (result) {
            let url;
            console.log(result.data);

            if (checkEditOrAdd.edit) {
              setEdit(true);
            } else if (checkEditOrAdd.add) {
              setIsAdd(true);
            }
            if (result.data.audioFile.length) {
              url = result.data.audioFile[0].url;

              setIsExistingAudio(true);
              hideModal();
              return setNewAudioBlob(url);
            } else {
              setShowNewRecordModal(true);
              setIsExistingAudio(false);
            }
            // console.log(result.data.audioFile);
            // result.data.audioFile.map((file) => {
            //   if (file.name === newWord.current.value) {
            //     console.log(file.url);
            //     console.log(file.name);
            //     url = file.url;
            //   }
            //   return url;
            // });
          }
        });
    } catch (error) {
      // setShowNewRecordModal(true);
      // setIsExistingAudio(false);
    }
  };

  const handleAddSubmit = () => {
    if (audioFromModal) {
      if (sounds.word) {
        newWord.current.value = sounds.word;
        setUndo({ word: sounds.word, position: selectWord.position });
        setRedo({ word: selectWord.word, position: selectWord.position });
        setCheckEditOrAdd({ edit: false, add: true });
        setSelectWord({
          word: sounds.word,
          position: selectWord.position,
          start: start,
          end: end,
        });
        if (checkEditOrAdd.edit) {
          setEdit(true);
        } else if (checkEditOrAdd.add) {
          setIsAdd(true);
        }
        hideModal();
        return setNewAudioBlob(sounds.url);
      }
      setSounds(null);
      setAudioFromModal(false);
    } else {
      const word = newWord.current.value;
      if (word) {
        setUndo({ word: selectWord.word, position: selectWord.position });
        setRedo({ word: undo.word, position: selectWord.position });
        setCheckEditOrAdd({ edit: false, add: true });
        setSelectWord({
          word: word,
          position: selectWord.position,
          start: start,
          end: end,
        });
        console.log(setSelectWord);
        checkIfAudioIsExisting(word);
      } else {
        toast.error("please enter word", options);
      }
    }
  };

  const handleEditSubmit = () => {
    if (audioFromModal) {
      if (sounds.word) {
        newWord.current.value = sounds.word;
        setUndo({ word: sounds.word, position: selectWord.position });
        setRedo({ word: selectWord.word, position: selectWord.position });
        setCheckEditOrAdd({ edit: true, add: false });
        setSelectWord({
          word: newWord.current.value,
          position: selectWord.position,
          start: start,
          end: end,
        });
        if (checkEditOrAdd.edit) {
          setEdit(true);
        } else if (checkEditOrAdd.add) {
          setIsAdd(true);
        }
        hideModal();
        return setNewAudioBlob(sounds.url);
      }
      setSounds(null);
      setAudioFromModal(false);
    } else {
      const word = newWord.current.value;
      if (word) {
        setUndo({ word: selectWord.word, position: selectWord.position });
        setRedo({ word: undo.word, position: selectWord.position });
        setCheckEditOrAdd({ edit: true, add: false });
        setSelectWord({
          word: word,
          position: selectWord.position,
          start: start,
          end: end,
        });
        checkIfAudioIsExisting(word);
      } else {
        toast.error("please enter word", options);
      }
    }
  };

  useEffect(() => {
    if (selectWord) {
      setStart(selectWord.start);
      setEnd(selectWord.end);
    }
    if (sounds.word) {
      newWord.current.value = sounds.word;
    }
  }, [sounds.word, selectWord]);

  return (
    <>
      <Modal centered show={showModify} size="sm" onHide={hideModal}>
        <Modal.Header closeButton>
          <Form.Label>Edit Word</Form.Label>
        </Modal.Header>
        <Modal.Body>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignContent: "center",
              flexGrow: 1,
            }}
          >
            <Box>
              <Form.Label>Set New Word</Form.Label>
            </Box>
            <Box>
              <Form.Group>
                <Box sx={{ display: "flex", displayDirection: "row" }}>
                  <Box sx={{ paddingRight: "10px" }}>
                    <Form.Control
                      type="text"
                      ref={newWord}
                      placeholder={selectWord.word}
                    />
                  </Box>
                  <Box>
                    <Button
                      onClick={deleteWord}
                      variant="contained"
                      color="secondary"
                    >
                      Remove
                    </Button>
                  </Box>
                </Box>
                <Box
                  sx={{
                    p: 1,
                    flexGrow: 1,
                    display: "flex",
                    justifyContent: "center",
                    width: "100%",
                  }}
                >
                  <Button
                    style={{ alignSelf: "center" }}
                    variant="outlined"
                    color="primary"
                    onClick={() => setShowAvailableAudioSound(true)}
                  >
                    View Available Audio
                  </Button>
                </Box>
              </Form.Group>
            </Box>
          </Box>
        </Modal.Body>
        <Modal.Footer>
          <Box sx={{ display: "flex", flexDirection: "row" }}>
            <Box sx={{ paddingRight: "10px" }}>
              {selectWord.word === " " ? (
                <Button
                  color="primary"
                  variant="contained"
                  onClick={handleAddSubmit}
                >
                  Add New Word
                </Button>
              ) : (
                <Button
                  color="primary"
                  variant="contained"
                  onClick={handleEditSubmit}
                >
                  Replace
                </Button>
              )}
            </Box>
            <Box>
              <Button color="secondary" variant="outlined" onClick={hideModal}>
                Cancel
              </Button>
            </Box>
          </Box>
        </Modal.Footer>
      </Modal>

      {isExistingAudio ? null : (
        <RecordNewAudioForNewWord
          show={showNewRecordModal}
          setIsAdd={setIsAdd}
          setEdit={setEdit}
          checkEditOrAdd={checkEditOrAdd}
          setNewAudioBlob={setNewAudioBlob}
          selectWord={selectWord}
          user={user}
          setShowNewRecordModal={setShowNewRecordModal}
          hideModal={hideModal}
        />
      )}
      <AvailableSoundsModal
        show={showAvailableAudioSound}
        setAudioFromModal={setAudioFromModal}
        setSounds={setSounds}
        user={user}
        setShowAvailableSounds={setShowAvailableAudioSound}
      />
    </>
  );
}

export default EditModal;
