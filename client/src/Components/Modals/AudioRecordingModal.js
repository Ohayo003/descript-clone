import React, { useState, useEffect } from "react";
import { Modal, Container } from "react-bootstrap";
import { IconButton, Box } from "@material-ui/core";
import StopIcon from "@material-ui/icons/Stop";
import PauseIcon from "@material-ui/icons/Pause";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import { useReactMediaRecorder } from "react-media-recorder";

function AudioRecordingModal({
  show,
  setBlobFile,
  setIsRecording,
  setUploadStatus,
}) {
  const [hideModal, setHideModal] = useState(false);
  const {
    startRecording,
    stopRecording,
    pauseRecording,
    status,
    resumeRecording,
    mediaBlobUrl,
  } = useReactMediaRecorder({ audio: true });

  const handleStopRecording = () => {
    stopRecording();
    console.log(mediaBlobUrl);
  };

  useEffect(() => {
    if (mediaBlobUrl && status === "stopped") {
      setHideModal(true);
      setUploadStatus(true);
      setIsRecording(false);
      return setBlobFile(mediaBlobUrl);
    }
  }, [mediaBlobUrl, setUploadStatus, setIsRecording, setBlobFile, status]);

  return (
    <>
      <Modal
        backdrop={false}
        show={show}
        onHide={hideModal}
        animation={true}
        size="sm"
      >
        <Container>
          <Box
            sx={{ display: "flex", displayDirection: "row", flexWrap: "wrap" }}
          >
            <Box sx={{ flexGrow: 1, p: 1, paddingTop: "12px" }}>
              <h6>{status}</h6>
            </Box>
            <Box sx={{ p: 1 }}>
              <IconButton
                size="small"
                onClick={
                  status === "recording"
                    ? pauseRecording
                    : status === "paused"
                    ? resumeRecording
                    : startRecording
                }
              >
                {status === "recording" ? (
                  <PauseIcon color="error" />
                ) : status === "paused" ? (
                  <PlayArrowIcon color="primary" />
                ) : (
                  <PlayArrowIcon color="primary" />
                )}
              </IconButton>
              <IconButton
                size="small"
                onClick={handleStopRecording}
                disabled={status === "idle" ? true : false}
              >
                <StopIcon color="error" />
              </IconButton>
            </Box>
          </Box>
        </Container>
      </Modal>
    </>
  );
}

export default AudioRecordingModal;
