import React, { useState } from "react";
import { Modal, Card } from "react-bootstrap";
import Typography from "@mui/material/Typography";
import ComputerIcon from "@mui/icons-material/Computer";
import HomeIcon from "@mui/icons-material/Home";
import "./ShowOptions.css";
import WorkSpaceDriveModal from "./WorkSpaceDriveModal";
import LocalDrive from "./LocalDrive";
import { Box } from "@mui/system";

function ShowOptions({
  show,
  setShowOptions,
  currentProject,
  setCurrentProject,
  setRecordId,
  setIsWorkSpaceFile,
  user,
  // setRecordingMedia,
}) {
  const [showWorkspaceDrive, setShowWorkspaceDrive] = useState(false);
  const [showLocalDrive, setShowLocalDrive] = useState(false);
  const handleClose = () => setShowOptions(false);
  const handleWorkSpaceUpload = () => setShowWorkspaceDrive(true);

  return (
    <>
      <Modal show={show} onHide={handleClose} animation={true} centered>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Choose Location
          </Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ padding: "30px" }}>
          <Box
            sx={{
              p: 1,
              display: "flex",
              flexDirection: "row",
              flexWrap: "nowrap",
              justifyContent: "space-around",
            }}
          >
            <Box>
              <Card
                border="secondary"
                className="local-drive"
                onClick={() => setShowLocalDrive(true)}
              >
                <ComputerIcon fontSize="large" className="col-md-5 mx-auto" />
                <Typography variant="h6" className="btn-labels">
                  Local Drive
                </Typography>
              </Card>
            </Box>
            <Box>
              <Card
                border="secondary"
                className="work-space-drive"
                onClick={handleWorkSpaceUpload}
              >
                <HomeIcon fontSize="large" className="col-md-5 mx-auto" />
                <Typography variant="h6" className="btn-labels">
                  Work Space
                </Typography>
              </Card>
            </Box>
          </Box>
        </Modal.Body>
      </Modal>

      <WorkSpaceDriveModal
        user={user}
        show={showWorkspaceDrive}
        setCloseModal={setShowWorkspaceDrive}
        setShowOptions={setShowOptions}
        handleCloseOptions={handleClose}
        setIsWorkSpaceFile={setIsWorkSpaceFile}
        setRecordId={setRecordId}
        currentProject={currentProject}
        // setRecoringMedia={setRecordingMedia}
      />
      <LocalDrive
        // setMediaFile={setMediaFile}
        setCurrentProject={setCurrentProject}
        currentProject={currentProject}
        user={user}
        showDrive={showLocalDrive}
        setShowLocalDrive={setShowLocalDrive}
        setShowOptions={setShowOptions}
      />
    </>
  );
}

export default ShowOptions;
