import { Stack } from "@mui/material";
import React, { useRef } from "react";
import { Modal, Form, Button } from "react-bootstrap";
import { toast } from "react-toastify";
import axios from "../../axios";
import { options } from "../../ToastifyOptions";
import Typography from "@mui/material/Typography";

function AddProjectModal({
  owner,
  showAddModal,
  setShowAddModal,
  setProjectID,
  setMyProjects,
}) {
  const handleClose = () => setShowAddModal(false);
  const projectName = useRef();

  //Creates a project file
  const handleCreateProject = async (e) => {
    e.preventDefault();
    const title = projectName.current.value;
    const data = {
      email: owner,
      projectName: title,
    };
    try {
      if (title) {
        handleClose();
        const newProject = axios.post("/projects/create-project-folder", data);
        await toast
          .promise(
            newProject,
            {
              pending: `Creating Project ${data.projectName}... `,
              error: `Failed to Create Project ${data.projectName}`,
              success: `Project ${data.projectName} Created!`,
            },
            options
          )
          .then((result) => {
            if (result) {
              console.log(result);
              setMyProjects(false);
              setProjectID(result.data._id);
            }
          })
          .catch((error) => console.error(error));
      } else {
        toast.error("Please Enter Project Name");
      }
    } catch (error) {
      console.log(error);
      toast.error(`Failed to add project ${error}`);
    }
  };
  const enterKey = (e) => {
    e.preventDefault();
    const title = projectName.current.value;
    const key = e.key;
    if (key === "Enter") {
      if (title) {
        alert(title);
      } else {
        toast.error("Please enter Project Name", options);
      }
    } else {
      toast.error("Please enter Project Name", options);
    }
  };

  return (
    <>
      <Modal show={showAddModal} onHide={handleClose} animation={true} centered>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            New Project
          </Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ padding: "30px" }}>
          <Form.Control
            type="text"
            ref={projectName}
            placeholder="Untitled Project"
            size="sm"
            // onKeyPress={enterKey}
            style={{ padding: "25px 5px 15px 20px" }}
          />
        </Modal.Body>
        <Modal.Footer>
          <Stack direction="row" gap={15} style={{ textAlign: "center" }}>
            <Typography
              variant="caption"
              style={{ fontSize: "11px", color: "GrayText" }}
            >
              This project is in your personal Workspace
            </Typography>
            <Button
              size="sm"
              onClick={handleCreateProject}
              onKeyDown={enterKey}
            >
              Create Project
            </Button>
          </Stack>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default AddProjectModal;
