import React, { useState } from "react";
import { Form, Modal } from "react-bootstrap";
import { toast } from "react-toastify";
import { options } from "../../ToastifyOptions";
import axios from "../../axios";
import Box from "@mui/material/Box";
import { Button } from "@material-ui/core";
import CloudUploadIcon from "@mui/icons-material/CloudUpload";

function LocalDrive({
  setShowLocalDrive,
  showDrive,
  user,
  currentProject,
  setCurrentProject,
  setShowOptions,
}) {
  const [file, setFile] = useState();
  const handleClose = () => setShowLocalDrive(false);
  const handleCloseOptions = () => setShowOptions(false);

  const handleChangeFile = (files) => {
    console.log(files);
    setFile(files);
  };
  // console.log(file);
  const handleUploadMedia = async () => {
    // setLocalFile(file);
    // setIsLocalDriveFile(true);
    if (file) {
      handleClose();
      handleCloseOptions();
      const form_data = new FormData();
      form_data.append("media", file);
      const config = {
        headers: { "Content-Type": "multipart/form-data" },
      };
      if (currentProject) {
        console.log(file.name);
        console.log("current: ", currentProject);
        const uploadMedia = axios.put(
          `/projects/file-upload/transcript-video/${user._id}/${currentProject._id}`,
          form_data,
          config
        );
        await toast
          .promise(
            uploadMedia,
            {
              pending: `Uploading Media. Please wait`,
              success: `Upload Successful`,
              error: `Uploading Failed`,
            },
            options
          )
          .then((result) => {
            setCurrentProject(result.data);
            console.log(result);
          })
          .catch((error) => console.error(error));
      }
    } else {
      toast.error("Please Select file", options);
    }
  };

  return (
    <>
      <Modal size="sm" show={showDrive} onHide={handleClose} centered>
        <Modal.Header>
          <Modal.Title>SELECT FILE</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Box
            sx={{
              p: 1,
              display: "flex",
              flexDirection: "column",
              widht: "100%",
            }}
          >
            <Box sx={{ width: "100%", flexGrow: 1 }}>
              <Form.Group>
                <Form.Control
                  type="file"
                  name="media"
                  onChange={(e) => {
                    handleChangeFile(e.target.files[0]);
                  }}
                />
              </Form.Group>
            </Box>
            <br />
            <Box
              sx={{
                flexGrow: 1,
                width: "100%",
                alignContent: "center",
              }}
            >
              <Button
                variant="contained"
                color="primary"
                type="submit"
                style={{ width: "100%" }}
                onClick={handleUploadMedia}
              >
                {" "}
                <CloudUploadIcon style={{ paddingRight: "5px" }} /> Upload File
              </Button>
            </Box>
          </Box>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default LocalDrive;
