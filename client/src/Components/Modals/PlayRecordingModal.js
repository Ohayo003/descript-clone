import React from "react";
import { Modal } from "react-bootstrap";
import Box from "@material-ui/core/Box";

function PlayRecordingModal({ playRecording, show, setShow }) {
  const handleHide = () => setShow(false);
  return (
    <>
      <Modal centered size="lg" show={show} onHide={handleHide} animation>
        <Modal.Header closeButton>
          {playRecording && show ? playRecording.name : null}
        </Modal.Header>
        <Modal.Body>
          <Box
            sx={{
              alignContent: "center",
              width: "100%",
            }}
          >
            <video
              src={playRecording && show ? playRecording.url : null}
              controls
              autoPlay
              width="750"
              height="350"
            />
          </Box>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default PlayRecordingModal;
