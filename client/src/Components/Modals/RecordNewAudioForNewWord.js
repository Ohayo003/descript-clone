import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
// import Button from "@material-ui/core/Button";
import { IconButton } from "@material-ui/core";
import { useReactMediaRecorder } from "react-media-recorder";
import MicNone from "@mui/icons-material/MicNone";
import Done from "@mui/icons-material/Done";
import { Box } from "@mui/system";
import axios from "../../axios";
import { toast } from "react-toastify";
import { options } from "../../ToastifyOptions";

function RecordNewAudioForNewWord({
  show,
  setNewAudioBlob,
  setShowNewRecordModal,
  user,
  checkEditOrAdd,
  selectWord,
  setEdit,
  setIsAdd,
  hideModal,
}) {
  const { startRecording, stopRecording, status, mediaBlobUrl } =
    useReactMediaRecorder({ audio: true });
  const [blob, setBlob] = useState("");

  const handleHide = () => {
    setShowNewRecordModal(false);
    hideModal();
  };

  const handleStopRecording = () => {
    stopRecording();
    // console.log(mediaBlobUrl);
    if (blob) {
      console.log(blob);
      if (checkEditOrAdd.edit) {
        setEdit(true);
      }
      if (checkEditOrAdd.add) {
        setIsAdd(true);
      }

      trainNewAudio(blob);
      handleHide();
    }
  };
  useEffect(() => {
    if (mediaBlobUrl && status === "stopped") {
      return setBlob(mediaBlobUrl);
    }
  }, [mediaBlobUrl, status]);

  const trainNewAudio = async (blobUrl) => {
    let blobFile;
    console.log(`new Word ${selectWord.word}`);
    const formData = new FormData();
    const config = {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };
    try {
      if (blobUrl) {
        await fetch(blobUrl)
          .then((res) => res.blob())
          .then((blobData) => {
            return (blobFile = blobData);
          });
      }
      formData.append("audioFile", blobFile);
      formData.append("name", selectWord.word);

      const trainAudio = axios.post(
        `/train-audio/audio/${user._id}`,
        formData,
        config
      );
      await toast
        .promise(
          trainAudio,
          {
            pending: "Training New Audio. Please wait...",
            success: "Training Audio successful",
            error: "Failed to Train Audio",
          },
          options
        )
        .then(async (result) => {
          if (result) {
            let url;
            console.log(result.data);
            console.log(result.data.url);
            result.data.audioFile.map((audio) => {
              if (audio.name === selectWord.word) {
                url = audio.url;
              }
              return url;
            });
            setNewAudioBlob(url);
          }
        });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Modal centered show={show} onHide={handleHide}>
      <Modal.Body>
        <Box
          sx={{
            p: 1,
            display: "flex",
            flexDirection: "row",
            flexWrap: "nowrap",
          }}
        >
          <Box sx={{ flexGrow: 1 }}>Record Audio for Word</Box>
          <Box>
            <IconButton size="small" onClick={startRecording}>
              <MicNone color="primary" />
            </IconButton>
          </Box>
          <Box>
            <IconButton
              disabled={status === "idle" ? true : false}
              size="small"
              onClick={handleStopRecording}
            >
              <Done color="success" />
            </IconButton>
          </Box>
        </Box>
      </Modal.Body>
    </Modal>
  );
}

export default RecordNewAudioForNewWord;
