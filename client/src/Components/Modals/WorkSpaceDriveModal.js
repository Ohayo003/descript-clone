import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import Box from "@mui/material/Box";
import "./WorkSpaceDrive.css";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import moment from "moment";
// import { Button } from "@material-ui/core";
import axios from "../../axios";
// import { toast } from "react-toastify";

function WorkSpaceDriveModal({
  show,
  setCloseModal,
  user,
  setRecordId,
  setIsWorkSpaceFile,
  handleCloseOptions,
}) {
  const [mediaFiles, setMediaFiles] = useState();

  const handleClose = () => setCloseModal(false);

  const handleSelectMedia = async (id) => {
    // alert(id);
    if (id) {
      setRecordId(id);
      setIsWorkSpaceFile(true);
      handleClose();
      handleCloseOptions();
    }
  };

  useEffect(() => {
    try {
      async function getData() {
        if (show) {
          console.log(show);
          await axios
            .get(`/projects/files/get-video-recordings/${user._id}`)
            .then((result) => {
              if (result) {
                console.log(result);
                return setMediaFiles(result.data);
              } else {
                console.log("error");
              }
            });
        }
      }
      getData();
    } catch (error) {
      console.log(error);
    }
  }, [user._id, show]);

  return (
    <Modal
      show={show}
      onHide={handleClose}
      animation={true}
      size="xl"
      centered
      className="workspace-drive"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Workspace Drive
        </Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ padding: "30px" }}>
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            flexDirection: "row",
            alignContent: "start",
            p: 1,
            m: 1,
            bgcolor: "background.paper",
            maxWidth: 1000,
          }}
        >
          {show && mediaFiles && mediaFiles.files.media.length ? (
            mediaFiles.files.media.map((recordings, index) => (
              <Box sx={{ p: 1 }} key={index}>
                <Card
                  key={index}
                  border="secondary"
                  className="workspace-drive-videos"
                  onMouseDown={() => handleSelectMedia(recordings.id)}
                >
                  <CardMedia
                    className="preview-container"
                    component="video"
                    alt="Recorded Video"
                    src={recordings.url}
                  />

                  <CardContent>
                    <Typography gutterBottom variant="caption">
                      {recordings.name} -{" "}
                      {moment(recordings.createdAt).format("YYYY-MM-DD")}
                    </Typography>
                  </CardContent>
                </Card>
              </Box>
            ))
          ) : (
            <Typography variant="h5" className="h5-no-video">
              There are currently no Recording Video's in the Workspace
            </Typography>
          )}
        </Box>
      </Modal.Body>
    </Modal>
  );
}

export default WorkSpaceDriveModal;
