import React, { useContext, useRef } from "react";
import { Container, Navbar, Nav, Tooltip } from "react-bootstrap";
import Logo from "../../assets/images/descript_logo.png";
import { AuthContext } from "../Context/AuthContext";
import { sign_out } from "../Context/AuthenticationCall";
import AccountCircleRoundedIcon from "@material-ui/icons/AccountCircle";
import { OverlayTrigger, Popover } from "react-bootstrap";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import LogoutIcon from "@mui/icons-material/Logout";
import Typography from "@mui/material/Typography";
import VideoCameraFrontIcon from "@mui/icons-material/VideoCameraFront";
import { IconButton } from "@mui/material";

function Header({
  user,
  setProjectID,
  setRecordingID,
  setMyProjectsLink,
  setShowRecordModal,
  setShowCanvas,
  setShowControls,
  setTime,
  setTitleProject,
}) {
  const { dispatch } = useContext(AuthContext);
  const ref = useRef();

  const renderInfo = (props) => (
    <Tooltip id="button-tooltip-2" {...props}>
      Click to Record Video
    </Tooltip>
  );

  const handleShowRecord = () => {
    setShowRecordModal(true);
    setShowCanvas(true);
    setTime({
      minutes: 5,
      seconds: 0,
    });
    setShowControls(true);
  };

  const handleLogout = () => {
    sign_out(dispatch);
  };
  const defaultLink = () => {
    setMyProjectsLink(true);
  };
  return (
    <>
      <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand href="/home-page" onClick={defaultLink}>
            <img
              src={Logo}
              alt="Descript Logo"
              width="30"
              height="30"
              className="d-inline-block align-top"
            />{" "}
            Descript-clone
          </Navbar.Brand>
          <Navbar.Collapse className="justify-content-end">
            <Nav>
              <OverlayTrigger
                placement="bottom"
                delay={{ show: 250, hide: 400 }}
                overlay={renderInfo}
              >
                <IconButton
                  className="d-inline-flex align-items-center"
                  color="primary"
                  onClick={handleShowRecord}
                >
                  <VideoCameraFrontIcon fontSize="large" ref={ref} />
                </IconButton>
              </OverlayTrigger>
            </Nav>
            <Nav>
              <OverlayTrigger
                trigger="click"
                placement="bottom"
                rootClose={true}
                overlay={
                  <Popover id={`popover-positioned-bottom`}>
                    <Popover.Header>
                      <Typography variant="h5">
                        {user.firstName} {user.lastName}
                      </Typography>
                    </Popover.Header>
                    <Popover.Body>
                      <Box sx={{ display: "flex", flexDirection: "column" }}>
                        <Button
                          variant="text"
                          onClick={handleLogout}
                          style={{
                            justifyContent: "space-between",
                            textAlign: "center",
                          }}
                        >
                          <LogoutIcon fontSize="small"></LogoutIcon>Logout
                        </Button>
                      </Box>
                    </Popover.Body>
                  </Popover>
                }
              >
                <AccountCircleRoundedIcon
                  style={{ cursor: "pointer" }}
                  fontSize="large"
                />
              </OverlayTrigger>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}

export default Header;
