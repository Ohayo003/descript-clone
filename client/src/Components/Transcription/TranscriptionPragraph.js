import React from "react";
import TranscriptionWord from "./TranscriptionWord";

function TranscriptionPragraph({
  data,
  setSelectWord,
  setShowModify,
  showModify,
  setIsAdd,
  isAdd,
  selectWord,
  isTranscribed,
  setIsUndo,
  user,
  setIsRedo,
  isUndo,
  playBack,
  setRedo,
  setUndo,
  setNewAudioBlob,
  undo,
  redo,
  isPlaying,
  setIsDelete,
  setIsEdit,
  isEdit,
  setIsExistingAudio,
  isDelete,
}) {
  return (
    <>
      {data && (
        <p>
          {data.map((sentence, index) => (
            <TranscriptionWord
              isTranscribed={isTranscribed}
              setIsUndo={setIsUndo}
              setIsExistingAudio={setIsExistingAudio}
              setIsRedo={setIsRedo}
              setRedo={setRedo}
              setUndo={setUndo}
              playBack={playBack}
              isPlaying={isPlaying}
              isUndo={isUndo}
              setIsAdd={setIsAdd}
              isAdd={isAdd}
              user={user}
              undo={undo}
              setNewAudioBlob={setNewAudioBlob}
              redo={redo}
              setShowModify={setShowModify}
              setIsDelete={setIsDelete}
              setIsEdit={setIsEdit}
              isDelete={isDelete}
              isEdit={isEdit}
              setSelectWord={setSelectWord}
              showModify={showModify}
              selectWord={selectWord}
              data={sentence}
              key={index}
            />
          ))}
        </p>
      )}
    </>
  );
}

export default TranscriptionPragraph;
