import React, { useState, useEffect } from "react";
// import { Card, Container } from "react-bootstrap";
import { Box } from "@mui/system";
import axios from "../../axios";
import WaveSurfer from "./WaveSurfer";
import TranscriptionPragraph from "./TranscriptionPragraph";
import VideoPlayer from "./VideoPlayer";
import RedoIcon from "@mui/icons-material/Redo";
import UndoIcon from "@mui/icons-material/Undo";
import { IconButton } from "@material-ui/core";
import Typography from "@mui/material/Typography";
import { toast } from "react-toastify";
import { options } from "../../ToastifyOptions";

function TranscriptionWrapper({
  currentProject,
  setCurrentProject,
  isTranscribed,
  transcriptData,
  user,
  setPlayBack,
  playBack,
  setPlaying,
  setWave,
  playing,
}) {
  const [fileType, setFileType] = useState(null);
  const [isDelete, setIsDelete] = useState(false);
  const [undo, setUndo] = useState({
    word: "",
    position: 0,
    url: "",
    transcriptUrl: "",
    operation: "",
  });
  const [redo, setRedo] = useState({
    word: "",
    position: 0,
    url: "",
    transcriptUrl: "",
  });
  const [isUndo, setIsUndo] = useState(false);
  const [isExistingAudio, setIsExistingAudio] = useState(false);
  const [isRedo, setIsRedo] = useState(false);
  const [videoUrl, setVideoUrl] = useState("");
  const [isAdd, setIsAdd] = useState(false);
  const [newAudioBlob, setNewAudioBlob] = useState("");
  const [isEdit, setIsEdit] = useState(false);
  const [selectWord, setSelectWord] = useState({
    position: 0,
    word: "",
    confidence: 0,
    start: 0,
    end: 0,
  });
  const [showModify, setShowModify] = useState(false);

  //Undo Changes
  const undoChanges = async () => {
    let fileBlob, mediaBlob;
    if (isUndo) {
      console.log(
        undo.word,
        undo.position,
        `Media: ${undo.url}`,
        `transcript: ${undo.transcriptUrl}`
      );
      // transcriptData[0].words[undo.position].value = undo.word;
      setRedo({
        word: undo.word,
        position: undo.position,
        url: undo.url,
        transcriptUrl: undo.transcriptUrl,
      });

      console.log(
        `word - ${redo.word}, position - ${redo.position}, url - ${redo.url}, transcriptUrl - ${redo.transcriptUrl}`
      );
      await fetch(undo.url)
        .then((res) => res.blob())
        .then((data) => {
          return (mediaBlob = data);
        });

      await fetch(undo.transcriptUrl)
        .then((res) => res.blob())
        .then((data) => {
          return (fileBlob = data);
        });

      const formData = new FormData();
      const config = {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      };
      try {
        formData.append("audio", mediaBlob);
        formData.append("transcriptData", fileBlob);
        formData.append("word", undo.word);
        formData.append("operation", undo.operation);
        formData.append("start", selectWord.start);
        formData.append("end", selectWord.end);
        formData.append("confidence");
        formData.append("index", undo.position);

        const undoChanges = axios.put(
          `/projects/edit/changes/undo-redo-changes/${currentProject._id}`,
          formData,
          config
        );

        await toast
          .promise(
            undoChanges,
            {
              pending: "reverting back before changes happened. please wait...",
              success: "changes succesfully reverted.",
              error: "Failed to undo changes.",
            },
            options
          )
          .then((result) => {
            setIsUndo(false);
            setIsRedo(true);
            setUndo(null);
            setCurrentProject(result.data);
          });
      } catch (error) {
        console.log(error);
      }
    }
  };
  //Redo Changes
  const redoChanges = async () => {
    console.log(redo.position);
    if (isRedo) {
      const formData = new FormData();
      const config = {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      };
      try {
        formData.append("audio", redo.url);
        formData.append("transcriptData", redo.transcriptUrl);
        formData.append("word", redo.word);
        formData.append("index", redo.position);

        const redoChanges = axios.put(
          `/projects/edit/changes/undo-redo-changes/${currentProject._id}`,
          formData,
          config
        );

        await toast
          .promise(
            redoChanges,
            {
              pending: "reverting back before changes happened. please wait...",
              success: "changes succesfully reverted.",
              error: "Failed to undo changes.",
            },
            options
          )
          .then((result) => {
            setIsUndo(true);
            setIsRedo(false);
            return setCurrentProject(result.data);
          });
      } catch (error) {
        console.log(error);
      }
      // transcriptData[0].words.splice(redo.position, 1, redo.word);
    }
  };

  //Delete an audio by removing text
  useEffect(() => {
    if (isDelete) {
      const deleteAudio = async () => {
        let blobData, transData, duration;
        if (currentProject) {
          const url = currentProject.files.media[0].url;
          const transUrl = currentProject.files.transcription[0].url;

          setUndo({
            word: selectWord.word,
            position: selectWord.position,
            url: url,
            transcriptUrl: transUrl,
            operation: "delete",
          });
          if (currentProject.files.transcription) {
            await fetch(transUrl)
              .then((res) => res.blob())
              .then((data) => {
                return (transData = data);
              });
          }

          await fetch(url)
            .then((res) => res.blob())
            .then((data) => {
              return (blobData = data);
            });

          const formData = new FormData();
          formData.append("original", blobData);
          formData.append("transcriptData", transData);
          formData.append("word", selectWord.word);
          formData.append("index", selectWord.position);
          formData.append("operation", undo.operation);
          formData.append("start", selectWord.start);
          formData.append("end", selectWord.end);
          formData.append("id", currentProject.files.media[0].id);
          formData.append("duration", duration);

          const config = {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          };
          const deleteDataAudio = axios.put(
            `/projects/edit/delete-audio/${currentProject._id}`,
            formData,
            config
          );
          await toast
            .promise(deleteDataAudio, {
              pending: "Updating Audio File. Please wait.",
              success: "Audio File Updated Successful",
              error: "Failed to Update Audio File.",
            })
            .then((result) => {
              if (result) {
                console.log(result.data);
                setCurrentProject(result.data);
                setIsDelete(false);
              }
              setIsDelete(false);
            });
          setIsDelete(false);
        }
      };
      deleteAudio();
      setIsDelete(false);
    }
  }, [
    selectWord.word,
    currentProject,
    undo.position,
    undo.word,
    undo.url,
    undo.transcriptUrl,
    undo.operation,
    isDelete,
    setCurrentProject,
    selectWord.end,
    selectWord.start,
    selectWord.position,
  ]);

  //Add audio by adding a new text
  useEffect(() => {
    console.log(`add: ${isAdd}`);
    if (isAdd && newAudioBlob) {
      if (currentProject) {
        const url = currentProject.files.media[0].url;
        let transData;
        let position = selectWord.position;
        // console.log("url is: ", undo.url, "original url: ", url);
        const start =
          transcriptData[0].words[position - 1].end_ts !== undefined
            ? transcriptData[0].words[position - 1].end_ts
            : transcriptData[0].words[position - 2].end_ts;
        const end =
          transcriptData[0].words[position + 1].ts !== undefined
            ? transcriptData[0].words[position + 1].ts
            : transcriptData[0].words[position + 2].ts;

        console.log("new start: ", start);
        console.log("new End: ", end);
        const addAudio = async () => {
          let newAudioBlobData;
          let blobData;
          let duration;
          console.log(selectWord.position);

          if (currentProject.files.transcription) {
            await fetch(currentProject.files.transcription[0].url)
              .then((res) => res.blob())
              .then((data) => {
                return (transData = data);
              });
          }
          await fetch(newAudioBlob)
            .then((res) => res.blob())
            .then((dataBlob) => {
              console.log(dataBlob);
              return (newAudioBlobData = dataBlob);
            })
            .catch((err) => {
              throw err;
            });

          await fetch(url)
            .then((res) => res.blob())
            .then((data) => {
              return (blobData = data);
            });
          console.log(`undo position:  ${selectWord.position}`);
          setUndo({
            word: selectWord.word,
            position: selectWord.position,
            url: url,
            transcriptUrl: currentProject.files.transcription[0].url,
            operation: "add",
          });

          console.log(newAudioBlobData);
          const formData = new FormData();
          console.log("new start: ", start);
          console.log("new End: ", end);

          formData.append("original", blobData);
          formData.append("newAudio", newAudioBlobData);
          formData.append("transcriptData", transData);
          formData.append("index", selectWord.position);
          formData.append("newWord", selectWord.word);
          formData.append("operation", undo.operation);
          formData.append("start", start);
          formData.append("end", end);
          formData.append("id", currentProject.files.media[0].id);
          formData.append("duration", duration);
          const config = {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          };
          const newWordAudio = axios.put(
            `/projects/edit/add-new-audio/${currentProject._id}`,
            formData,
            config
          );
          await toast
            .promise(
              newWordAudio,
              {
                pending: "Updating Audio File. Please Wait",
                success: "File Audio Updated Successful.",
                error: "Failed to Update File Audio",
              },
              options
            )
            .then((result) => {
              setCurrentProject(result.data);
              console.log(result);
              setIsAdd(false);
              setNewAudioBlob(null);
            });
          setIsAdd(false);
        };
        addAudio();

        setNewAudioBlob(null);
        setIsAdd(false);
      }
    }
  }, [
    selectWord.start,
    selectWord.end,
    selectWord.position,
    selectWord.word,
    setCurrentProject,
    transcriptData,
    newAudioBlob,
    currentProject,
    undo.operation,
    isAdd,
  ]);

  //Edit Audio by Replacin the text
  useEffect(() => {
    console.log(`edit: ${isEdit}`);
    if (isEdit && newAudioBlob) {
      const editAudio = async () => {
        let blobData;
        let newBlobData;
        let transData;
        let duration;
        // console.log(`start - ${selectWord.start} : end - ${selectWord.end}`);

        if (currentProject) {
          const url = currentProject.files.media[0].url;
          if (currentProject.files.transcription) {
            await fetch(currentProject.files.transcription[0].url)
              .then((res) => res.blob())
              .then((data) => {
                return (transData = data);
              });
          }

          await fetch(newAudioBlob)
            .then((res) => res.blob())
            .then((blobData) => {
              return (newBlobData = blobData);
            });

          console.log(newBlobData);

          await fetch(url)
            .then((res) => res.blob())
            .then((data) => {
              return (blobData = data);
            });

          setUndo({
            word: selectWord.word,
            position: selectWord.position,
            url: url,
            transcriptUrl: currentProject.files.transcription[0].url,
            operation: "edit",
          });

          // console.log(
          //   `word - ${undo.word}, position - ${undo.position}, url - ${undo.url}, transUrl - ${undo.transcriptUrl}`
          // );

          // console.log(blobData);
          const formData = new FormData();
          formData.append("original", blobData);
          formData.append("newAudio", newBlobData);
          formData.append("newWord", selectWord.word);
          formData.append("index", selectWord.position);
          formData.append("transcriptData", transData);
          formData.append("operation", undo.operation);
          formData.append("start", selectWord.start);
          formData.append("end", selectWord.end);
          formData.append("id", currentProject.files.media[0].id);
          formData.append("duration", duration);
          const config = {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          };
          const editAudio = axios.put(
            `/projects/edit/replace-word/${user._id}/${currentProject._id}`,
            formData,
            config
          );
          await toast
            .promise(
              editAudio,
              {
                pending: "Updating Audio File. Please Wait",
                success: "File Audio Updated Successful.",
                error: "Failed to Update File Audio",
              },
              options
            )
            .then((result) => {
              console.log(result);
              setCurrentProject(result.data);
              setNewAudioBlob(null);
              setIsEdit(false);
            });
        }
      };
      editAudio();

      setNewAudioBlob(null);
      setIsEdit(false);
    }
  }, [
    newAudioBlob,
    setCurrentProject,
    currentProject,
    isEdit,
    undo.operation,
    user._id,
    user.gender,
    transcriptData,
    selectWord,
  ]);

  //Check the File Type of media
  useEffect(() => {
    try {
      const checkFileType = async () => {
        if (currentProject && currentProject.files.media.length) {
          setVideoUrl(currentProject.files.media[0].url);
          const ext = currentProject.files.media[0].type;
          setFileType(ext);
        }
      };
      checkFileType();
    } catch (error) {
      console.log(error);
    }
  }, [currentProject]);

  return (
    <>
      <Box
        sx={{
          dispaly: "flex",
          flexDirection: "column",
          flexWrap: "wrap",
          width: "100%",
          height: "100%",
        }}
      >
        <Box
          sx={{
            justifyContent: "right",
            display: "flex",
            displayDirection: "row",
            p: 1,
          }}
        >
          <Box>
            <IconButton
              size="small"
              onClick={undoChanges}
              disabled={isUndo ? false : true}
            >
              <UndoIcon />
            </IconButton>
          </Box>
          <Box>
            <IconButton
              size="small"
              onClick={redoChanges}
              disabled={isRedo ? false : true}
            >
              <RedoIcon />
            </IconButton>
          </Box>
        </Box>
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            flexWrap: "nowrap",
            p: 1,
            flexGrow: 1,
            height: "80%",
            width: "100%",
          }}
        >
          <Box sx={{ p: 1, width: "100%" }}>
            {transcriptData && isTranscribed ? (
              <TranscriptionPragraph
                isTranscribed={isTranscribed}
                setIsUndo={setIsUndo}
                setIsExistingAudio={setIsExistingAudio}
                isExistingAudio={isExistingAudio}
                setIsRedo={setIsRedo}
                isPlaying={playing}
                undo={undo}
                playBack={playBack}
                setNewAudioBlob={setNewAudioBlob}
                setUndo={setUndo}
                isUndo={isUndo}
                setIsAdd={setIsAdd}
                isAdd={isAdd}
                user={user}
                setRedo={setRedo}
                data={transcriptData}
                setSelectWord={setSelectWord}
                setShowModify={setShowModify}
                selectWord={selectWord}
                showModify={showModify}
                setIsDelete={setIsDelete}
                setIsEdit={setIsEdit}
                isEdit={isEdit}
                isDelete={isDelete}
              />
            ) : (
              <Typography variant="h6" style={{ color: "lightgray" }}>
                File is not yet Transcripted.
              </Typography>
            )}
          </Box>
          {currentProject &&
          fileType === ("video/mp4" || "video/mkv" || "video/webm") ? (
            <Box>
              <VideoPlayer
                // handlePause={handlePause}
                url={videoUrl}
                // handlePlay={handlePlay}
                // handleEnd={handleEnd}
              />
            </Box>
          ) : null}
        </Box>
        <Box
          sx={{
            p: 1,
            height: "120px",
            alignItems: "end",
          }}
        >
          <WaveSurfer
            currentProject={currentProject}
            // handlePlay={handlePlay}
            // handlePause={handlePause}
            // handleEnd={handleEnd}
            selectWord={selectWord}
            isDelete={isDelete}
            setPlaying={setPlaying}
            playing={playing}
            setWave={setWave}
            currentPlayBack={setPlayBack}
            isEdit={isEdit}
          />
        </Box>
      </Box>
    </>
  );
}

export default TranscriptionWrapper;
