import React, { useEffect } from "react";
import { Box } from "@material-ui/core";
import { Container, Card } from "react-bootstrap";
import "./AudioView.css";
import { CircularProgress } from "@material-ui/core";
import MicNone from "@mui/icons-material/MicNone";
import VolumeUpIcon from "@mui/icons-material/VolumeUp";

function AudioRecordingView({
  isRecording,
  currentProject,
  uploadStatus,
  setUploadStatus,
  isTranscripting,
}) {
  useEffect(() => {}, []);
  return (
    <>
      <Container>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            flexWrap: "wrap",
            flexGrow: 1,
            p: 1,
            justifyContent: "start",
          }}
        >
          <Box>
            <Container>
              <Card className="podcast-transcript-text">
                {isRecording ? (
                  <Box
                    sx={{
                      p: 1,
                      display: "flex",
                      flexDirection: "row",
                      flexWrap: "wrap",
                    }}
                  >
                    <Box
                      sx={{
                        textAlign: "start",
                        paddingLeft: "10px",
                      }}
                    >
                      <MicNone color="inherit" />
                    </Box>

                    <Box
                      sx={{
                        paddingLeft: "10px",
                        flexGrow: 1,
                        textAlign: "start",
                      }}
                    >
                      <h6>
                        {currentProject && currentProject.projectName} -
                        Recording Audio...
                      </h6>
                    </Box>
                  </Box>
                ) : (
                  <>
                    <Box
                      sx={{
                        p: 1,
                        display: "flex",
                        flexDirection: "row",
                        flexWrap: "wrap",
                      }}
                    >
                      <Box
                        sx={{
                          textAlign: "start",
                          paddingLeft: "10px",
                        }}
                      >
                        <VolumeUpIcon color="inherit" />
                      </Box>

                      <Box
                        sx={{
                          paddingLeft: "10px",
                          flexGrow: 1,
                          textAlign: "start",
                        }}
                      >
                        <h6>
                          {currentProject && currentProject.projectName} -
                          Uploading Audio Recording...
                        </h6>
                      </Box>
                      <Box sx={{ paddingRight: "10px" }}>
                        <h6 className="transcripting-text">
                          {uploadStatus
                            ? "uploading"
                            : isTranscripting
                            ? "Transcripting"
                            : !uploadStatus || !isTranscripting
                            ? null
                            : "Complete"}
                        </h6>
                      </Box>
                      <Box>
                        {uploadStatus || isTranscripting ? (
                          <CircularProgress color="inherit" size="15px" />
                        ) : null}
                      </Box>
                    </Box>
                  </>
                )}
              </Card>
            </Container>
          </Box>
          <Box></Box>
        </Box>
      </Container>
    </>
  );
}

export default AudioRecordingView;
