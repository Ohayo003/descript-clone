import React from "react";

function VideoPlayer({ url, handlePlay, handlePause, handleEnd }) {
  return (
    <video
      src={url ? url : null}
      onPlaying={handlePlay}
      onPause={handlePause}
      onEnded={handleEnd}
      width="250"
    />
  );
}

export default VideoPlayer;
