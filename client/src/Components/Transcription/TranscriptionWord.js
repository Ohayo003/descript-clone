import React, { useEffect } from "react";
import { Box } from "@mui/system";
import { Container } from "react-bootstrap";
import Typography from "@mui/material/Typography";
import EditModal from "../Modals/EditModal";
// import classnames from "classnames";

function TranscriptionWord({
  data,
  isTranscribed,
  setSelectWord,
  setShowModify,
  showModify,
  selectWord,
  isEdit,
  isExistingAudio,
  setIsExistingAudio,
  setIsAdd,
  setUndo,
  isAdd,
  setNewAudioBlob,
  user,
  setIsRedo,
  isUndo,
  setIsUndo,
  isPlaying,
  undo,
  setRedo,
  isDelete,
  setIsDelete,
  setIsEdit,
  playBack,
}) {
  //Synchronized text with the playback of the audio file
  useEffect(() => {
    if (isPlaying) {
      data.words.forEach((item, index) => {
        if (playBack >= item.ts && playBack <= item.end_ts) {
          document.getElementById(index).style.background = "red";
          document.getElementById(index).style.color = "white";
        } else {
          document.getElementById(index).style.color = "black";
          document.getElementById(index).style.background = "transparent";
        }
      });
    }
  }, [isPlaying, playBack, data.words, data]);

  useEffect(() => {
    console.log("start: ", selectWord.start);
    console.log("end: ", selectWord.end);
    // console.log(data);
    if (isEdit) {
      setIsUndo(true);
      data.words[selectWord.position].value = ` ${selectWord.word} `;
    }
    if (isAdd) {
      setIsUndo(true);
      data.words[selectWord.position].value = ` ${selectWord.word} `;
    }
    if (isDelete) {
      setIsUndo(true);
      data.words.splice(selectWord.position, 1, selectWord.word);
    }
  }, [
    data.words,
    isEdit,
    selectWord,
    setIsUndo,
    setIsRedo,
    isUndo,
    isAdd,
    isDelete,
  ]);

  return (
    <>
      <Container style={{ scrollBehavior: "smooth" }}>
        <Box
          sx={{
            display: "flex",
            p: 1,
            flexDirection: "row",
            flexWrap: "nowrap",
          }}
        >
          {isTranscribed ? (
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                flexWrap: "nowrap",
                width: "100%",
                scrollBehavior: "smooth",
                overflowY: "scroll",
              }}
            >
              <Box sx={{ width: "90px" }}>
                {data && data.words.speaker ? (
                  data.words.speaker
                ) : (
                  <Typography variant="h7" style={{ fontWeight: "bold" }}>
                    Speaker :
                  </Typography>
                )}
              </Box>
              <Box sx={{ flexGrow: 1, paddingLeft: "10px", flexWrap: "wrap" }}>
                <Typography variant="paragraph">
                  {data &&
                    data.words.map((w, index) => (
                      <span
                        style={{
                          fontSize: "15px",
                          fontFamily: "sans-serif",
                          fontWeight: "bold",
                          cursor: "text",
                          // color: syncText(w.start, w.end, index),
                        }}
                        id={index}
                        className="words"
                        key={index}
                        onMouseDown={() => {
                          setSelectWord({
                            word: w.value,
                            position: index,
                            start: w.ts,
                            end: w.end_ts,
                            confidence: w.confidence,
                          });
                          setShowModify(true);
                          setIsDelete(false);
                          setIsAdd(false);
                          setIsEdit(false);
                        }}
                      >
                        {w.value}
                      </span>
                    ))}
                </Typography>
              </Box>
            </Box>
          ) : (
            <Box
              sx={{
                justifyContent: "center",
                textAlign: "center",
                p: 1,
                display: "flex",
                flexDirection: "row",
                flexGrow: 1,
                width: "100%",
              }}
            >
              <Typography variant="h5" style={{ color: "lightgray" }}>
                This file is not yet transcribed
              </Typography>
            </Box>
          )}
        </Box>
      </Container>
      <EditModal
        setShowModify={setShowModify}
        setDelete={setIsDelete}
        setEdit={setIsEdit}
        user={user}
        setUndo={setUndo}
        undo={undo}
        setNewAudioBlob={setNewAudioBlob}
        setIsAdd={setIsAdd}
        isExistingAudio={isExistingAudio}
        setRedo={setRedo}
        showModify={showModify}
        selectWord={selectWord}
        setSelectWord={setSelectWord}
        setIsExistingAudio={setIsExistingAudio}
      />
    </>
  );
}

export default TranscriptionWord;
