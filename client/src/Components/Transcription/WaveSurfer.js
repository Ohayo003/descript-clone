import React, { useEffect, useState } from "react";
import { Box } from "@material-ui/core";
import { Container, Card, ProgressBar } from "react-bootstrap";
import { CircularProgress } from "@material-ui/core";
import wave from "wavesurfer.js";
import "./WaveSurfer.css";
import AudioPlayer from "./AudioPlayer";

function WaveSurfer({
  currentProject,
  currentPlayBack,
  setPlaying,
  playing,
  setWave,
}) {
  const [waveSurfer, setWaveSurfer] = useState(null);
  const [player, setPlayer] = useState(false);
  const [duration, setDuration] = useState(0);
  const [playBack, setPlayBack] = useState(0);
  const [url, setUrl] = useState(null);
  // const [data, setData] = useState(null);

  useEffect(() => {
    setWaveSurfer(
      wave.create({
        container: "#waveform",
        fillParent: true,
        progressColor: "purple",
        normalize: true,
        waveColor: "violet",
        barHeight: 2,
        barWidth: 2,
        barGap: 1,
        hideScrollbar: true,
        backend: "MediaElement",
        height: "70",
      })
    );
  }, []);

  // useEffect(() =>)

  // useEffect(() => {
  //   if (waveSurfer) {
  //     waveSurfer.on("loading", () => {
  //       delete waveSurfer.backend.buffer;
  //       waveSurfer.unAll();
  //       waveSurfer.destroy();
  //       setPlayer(false);
  //     });

  //     waveSurfer.on("ready", () => {
  //       delete waveSurfer.backend.buffer;
  //       waveSurfer.unAll();
  //       waveSurfer.destroy();
  //       setPlayer(false);
  //     });
  //   }
  // }, [waveSurfer]);

  useEffect(() => {
    if (currentProject && currentProject.files.media.length) {
      setUrl(currentProject.files.media[0].url);
    }
    url ? waveSurfer.load(url) : setPlayer(true);
  }, [currentProject, url, waveSurfer]);

  useEffect(() => {
    if (waveSurfer !== null) {
      waveSurfer.on("waveform-ready", () => {
        setDuration(waveSurfer.getDuration());
        setWave(waveSurfer);
        setPlayer(true);
      });
      waveSurfer.on("play", () => {
        setPlaying(true);
      });

      waveSurfer.on("pause", () => {
        setPlaying(false);
      });

      waveSurfer.on("audioprocess", (progress) => {
        const shortProcess = progress.toFixed(1);

        if (shortProcess === playBack.toFixed(1)) {
          return;
        } else {
          currentPlayBack(progress);
          setPlayBack(progress);
        }
      });
    }
    if (waveSurfer) {
      return () => waveSurfer.un("waveform-ready");
    }
  }, [waveSurfer, playBack, currentPlayBack, setPlaying, setWave]);

  const minutes = (time) => {
    return Math.floor(time / 60);
  };
  const seconds = (time) => {
    return time > 60 ? (time % 60).toFixed(1) : time.toFixed(1);
  };

  return (
    <>
      <Container>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            flexWrap: "wrap",
            justifyContent: "start",
          }}
        ></Box>
        <Box>
          <Card className="wave-surfer-card">
            <div id="waveform"></div>
          </Card>
          <Box sx={{ paddingTop: "5px" }}>
            <ProgressBar
              style={{ height: "5px", alignSelf: "center" }}
              now={playBack}
              max={duration}
            />
          </Box>
          {waveSurfer && player ? (
            <>
              <Box sx={{ paddingTop: "5px" }}>
                <AudioPlayer wavesurfer={waveSurfer} isPlaying={playing} />
                <pre>
                  {minutes(playBack)}:{seconds(playBack)}s/
                  {waveSurfer && minutes(duration)}:
                  {waveSurfer && seconds(duration)}s
                </pre>
              </Box>
            </>
          ) : (
            <>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  flexWrap: "wrap",
                  justifyContent: "center",
                }}
              >
                <Box>
                  <pre>Loading Player...</pre>
                </Box>
                <Box>
                  <CircularProgress size="20px" color="inherit" />
                </Box>
              </Box>
            </>
          )}
        </Box>
      </Container>
    </>
  );
}

export default WaveSurfer;
