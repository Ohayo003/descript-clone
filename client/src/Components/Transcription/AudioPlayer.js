import React from "react";
import { IconButton } from "@material-ui/core";
import { Box } from "@mui/system";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import PauseIcon from "@mui/icons-material/Pause";
import SkipPreviousIcon from "@mui/icons-material/SkipPrevious";
import SkipNextIcon from "@mui/icons-material/SkipNext";
import StopIcon from "@mui/icons-material/Stop";

function AudioPlayer({ wavesurfer, isPlaying }) {
  return (
    <>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <Box sx={{ paddingLeft: "5px" }}>
          <IconButton onClick={() => wavesurfer.skipBackward()} size="small">
            <SkipPreviousIcon fontSize="medium" color="primary" />
          </IconButton>
        </Box>
        <Box sx={{ paddingLeft: "5px" }}>
          <IconButton
            onClick={
              isPlaying ? () => wavesurfer.pause() : () => wavesurfer.play()
            }
            size="small"
          >
            {isPlaying ? (
              <PauseIcon fontSize="medium" color="error" />
            ) : (
              <PlayArrowIcon fontSize="medium" color="primary" />
            )}
          </IconButton>
        </Box>
        <Box sx={{ paddingLeft: "5px" }}>
          <IconButton
            onClick={isPlaying ? () => wavesurfer.stop() : null}
            disabled={isPlaying ? false : true}
            size="small"
          >
            <StopIcon fontSize="medium" color="error" />
          </IconButton>
        </Box>
        <Box sx={{ paddingLeft: "5px" }}>
          <IconButton onClick={() => wavesurfer.skipForward()} size="small">
            <SkipNextIcon fontSize="medium" color="primary" />
          </IconButton>
        </Box>
      </Box>
    </>
  );
}

export default AudioPlayer;
