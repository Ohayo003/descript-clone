import React, { useContext, useEffect, useState } from "react";
import { Card, Container } from "react-bootstrap";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import ShowOptions from "../Modals/ShowOptions";
import axios from "../../axios";
import "./Transcription.css";
import { AuthContext } from "../Context/AuthContext";
import Box from "@mui/material/Box";
import { toast } from "react-toastify";
import { options } from "../../ToastifyOptions";
import { IconButton } from "@material-ui/core";
import DeleteIcon from "@mui/icons-material/Delete";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { CircularProgress } from "@material-ui/core";
import DoneIcon from "@mui/icons-material/Done";
import MicNoneIcon from "@mui/icons-material/MicNone";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import AudioRecordingModal from "../Modals/AudioRecordingModal";
import InsertDriveFileIcon from "@mui/icons-material/InsertDriveFile";
import TranscriptionWrapper from "./TranscriptionWrapper";
import PauseIcon from "@mui/icons-material/Pause";
import Typography from "@mui/material/Typography";
import AudioRecordingView from "./AudioRecordingView";

function DataTranscription({ projectID, setProjectID, setMyProjects }) {
  const { user } = useContext(AuthContext);
  const [showOptionModal, setShowOptionModal] = useState(false);
  const [transcribing, setTranscribing] = useState(false);
  const [currentProject, setCurrentProject] = useState();
  const [isRecording, setIsRecording] = useState(false);
  const [playBack, setPlayBack] = useState(0);
  const [isTranscripting, setIsTranscripting] = useState(false);
  const [wave, setWave] = useState(null);
  const [transcriptUrl, setTranscriptUrl] = useState("");
  const [playing, setPlaying] = useState(false);
  const [blobFile, setBlobFile] = useState("");
  const [fileUrl, setFileUrl] = useState("");
  const [transcribeData, setTranscribeData] = useState(false);
  const [localFile, setLocalFile] = useState("");
  const [isTranscribed, setIsTranscribed] = useState(false);
  const [isWorkSpaceFile, setIsWorkSpaceFile] = useState(false);
  const [uploadStatus, setUploadStatus] = useState(false);
  const [podcasting, setPodcasting] = useState(false);
  const [transcriptWords, setTranscriptionWords] = useState(null);
  const [recordId, setRecordId] = useState("");

  const start = () => {
    setIsRecording(true);
    setPodcasting(true);
  };

  //upload the audio if the blobFile is already available
  useEffect(() => {
    if (uploadStatus && currentProject) {
      console.log(blobFile);
      const handleUploadRecording = async () => {
        // setUploadStatus(true);
        try {
          let file;
          var formData = new FormData();
          let config = {
            headers: {
              "Content-type": "multipart/form-data",
            },
          };
          if (blobFile) {
            await fetch(blobFile)
              .then((res) => res.blob())
              .then((blob) => {
                return (file = blob);
              });
          }
          console.log(file);
          formData.append("Podcasting", file);
          formData.append("projectName", currentProject.projectName);

          await axios
            .post(
              `/projects/file-upload/podcasting-recordings/${user._id}`,
              formData,
              config
            )
            .then(async (result) => {
              if (result) {
                console.log(result.data);
                setUploadStatus(false);
                setIsTranscripting(true);
                if (result.data.files.media.length) {
                  return setFileUrl(result.data.files.media[0].url);
                }
              }
            });
        } catch (error) {
          console.log(error);
        }
      };
      handleUploadRecording();
    }
  }, [blobFile, user._id, currentProject, uploadStatus]);

  //Transcribe the File after uploading
  useEffect(() => {
    if (isTranscripting && currentProject) {
      console.log(fileUrl);
      const url = { videoURL: fileUrl };
      axios
        .put(
          `/projects/file-upload/transcribe-audio-video/${currentProject._id}`,
          url,
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        )
        .then((result) => {
          if (result) {
            console.log(result);
            setCurrentProject(result.data);
            setIsTranscribed(true);
            setTranscriptUrl(result.data.files.transcription[0].url);
            setIsTranscripting(false);
            setBlobFile(null);
            setPodcasting(false);
          }
          setIsTranscripting(false);
          setPodcasting(false);
        });
    }
  }, [isTranscripting, currentProject, fileUrl]);

  //get the transcript data onload
  useEffect(() => {
    let temp = [];
    let paragraph = {
      speaker: null,
      words: [],
    };
    try {
      // isTranscribed && currentProject &&
      if (isTranscribed && transcriptUrl) {
        const getTransFile = async () => {
          await axios.get(transcriptUrl).then((result) => {
            console.log(result.data);
            paragraph.speaker = result.data[0].speaker;
            result.data[0].elements.forEach((words) => {
              paragraph.words.push(words);
            });
          });
          // }
          temp.push(paragraph);
          setTranscriptionWords(temp);
        };
        getTransFile();
      }
    } catch (error) {
      console.log(error);
    }
  }, [isTranscribed, transcriptUrl]);

  //get the project by projectId on load
  useEffect(() => {
    console.log(projectID);
    try {
      if (projectID) {
        const fetchdata = async () => {
          const getProject = axios.get(`/projects/get-project/${projectID}`);

          await toast
            .promise(
              getProject,
              {
                pending: "Fetching Project Data...",
                success: "Successfully Fetched Projects Data.",
                error: "Failed to Fetch Projects Data",
              },
              options
            )
            .then((result) => {
              console.log(result);
              setIsTranscribed(result.data.transcribed);
              if (result.data.transcribed) {
                setTranscriptUrl(result.data.files.transcription[0].url);
              }
              setCurrentProject(result.data);
            })
            .catch((error) => console.log(error));
        };
        fetchdata();
      }
    } catch (error) {
      console.log(error);
    }
  }, [projectID]);

  //Upload file from workspace
  useEffect(() => {
    try {
      if (isWorkSpaceFile) {
        const uploadRecording = async () => {
          if (currentProject) {
            console.log(currentProject);
            if (recordId) {
              const data = {
                recordingId: recordId,
                projectId: currentProject._id,
              };
              const use_recordings = axios.put(
                `/projects/use-existing-recording/${user._id}`,
                data
              );

              await toast
                .promise(
                  use_recordings,
                  {
                    pending: "uploading file. please wait",
                    success: "upload successful",
                    error: "Faile to upload recording",
                  },
                  options
                )
                .then((result) => {
                  if (result) {
                    setCurrentProject(result.data);
                    setIsWorkSpaceFile(false);
                  }
                  // setIsWorkSpaceFile(false);
                });
            }
          }
        };
        uploadRecording();
      }
    } catch (error) {
      console.log(error);
    }
  }, [isWorkSpaceFile, recordId, user._id, currentProject]);

  //Transcript the file using Transcribe Button
  useEffect(() => {
    if (transcribeData) {
      const handleTranscription = async () => {
        setTranscribing(true);
        if (currentProject) {
          const url = { videoURL: currentProject.files.media[0].url };
          console.log(url);
          try {
            const transcribe = axios.put(
              `/projects/file-upload/transcribe-audio-video/${currentProject._id}`,
              url,
              {
                headers: {
                  "Content-Type": "application/json",
                },
              }
            );
            await toast
              .promise(
                transcribe,
                {
                  pending: "Transcribing Audio/Video. Please wait",
                  success: "Transcribing Successful",
                  error: "Failed to transcribe Audio/Video",
                },
                options
              )
              .then(async (result) => {
                if (result) {
                  console.log(result.data);
                  setTranscribeData(false);
                  setIsTranscribed(true);
                  setTranscribing(false);
                  setTranscriptUrl(result.data.files.transcription[0].url);
                  setCurrentProject(result.data);
                }
                setTranscribing(false);
                setTranscribeData(false);
              });
          } catch (error) {
            setTranscribing(false);
            console.log(error);
          }
        }
      };
      handleTranscription();
    }
  }, [transcribeData, currentProject]);

  //Delete the current project using projectId
  const handleDelete = async () => {
    try {
      if (currentProject) {
        console.log(currentProject._id);
        const deleteProject = axios.delete(
          `/projects/delete-projects/${user._id}/${currentProject._id}`
        );
        await toast
          .promise(
            deleteProject,
            {
              pending: `Deleting Project ${deleteProject.projectName}`,
              success: `Project Deleted!`,
              error: "Something went wrong while deleting the Project File",
            },
            options
          )
          .then(() => {
            setCurrentProject(null);
            setMyProjects(true);
            return setProjectID("");
          });
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleBack = () => {
    setMyProjects(true);
    setProjectID("");
    setCurrentProject(null);
  };

  const minutes = (time) => {
    return Math.floor(time / 60);
  };
  const seconds = (time) => {
    return time > 60 ? (time % 60).toFixed(1) : time.toFixed(1);
  };

  const renderRecordInfo = (props) => (
    <Tooltip id="button-tooltip-2" {...props}>
      Record Audio
    </Tooltip>
  );

  const renderPlayInfo = (props) => (
    <Tooltip id="button-tooltip-2" {...props}>
      Play Audio or Video
    </Tooltip>
  );

  const renderDeleteInfo = (props) => (
    <Tooltip id="button-tooltip-2" {...props}>
      Delete Project
    </Tooltip>
  );

  /// TODO: display wave controller for transcription editing
  // console.log(currentProject);
  return (
    <>
      <Card.Title className="card-title-transcription">
        Transcribe Audio/Video
      </Card.Title>
      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          flexDirection: "column",
          alignContent: "start",
          p: 1,
          m: 1,
          bgcolor: "background.paper",
          maxWidth: "100%",
        }}
      >
        <Container>
          <Card.Text>
            <Box
              sx={{
                display: "flex",
                flexWrap: "wrap",
                flexDirection: "row",
                maxWidth: "100%",
                justifyContent: "space-between",
              }}
            >
              <Box
                sx={{
                  p: 1,
                  flexDirection: "row",
                  display: "flex",
                  flexWrap: "wrap",
                }}
              >
                <Box sx={{ paddingRight: "5px" }}>
                  <Breadcrumbs>
                    <IconButton size="small" onClick={handleBack}>
                      <ArrowBackIcon fontSize="medium" />
                    </IconButton>
                  </Breadcrumbs>
                </Box>
                <Box>
                  <Breadcrumbs>
                    <Typography variant="h5">My Projects</Typography>
                  </Breadcrumbs>
                </Box>
                <Box sx={{ paddingTop: "3px" }}>
                  <Breadcrumbs>
                    <NavigateNextIcon fontSize="small" />
                  </Breadcrumbs>
                </Box>
                <Box>
                  <Breadcrumbs>
                    <Typography variant="h6">
                      {currentProject && currentProject.projectName}
                    </Typography>
                  </Breadcrumbs>
                </Box>
                {/* <h5 style={{ textAlign: "left" }}>
                  {currentProject && currentProject.projectName}
                </h5> */}
              </Box>
              <Box
                sx={{
                  p: 1,
                  display: "flex",
                  flexDirectio: "row",
                  flexGrow: 1,
                  flexWrap: "wrap",
                  justifyContent: "center",
                }}
              >
                <Box sx={{ marginRight: "5px" }}>
                  <Typography variant="h5">
                    {playBack
                      ? `${minutes(playBack)}:${seconds(playBack)}`
                      : "00:00:00"}
                  </Typography>
                </Box>
                <Box>
                  <OverlayTrigger
                    placement="bottom"
                    delay={{ show: 100, hide: 100 }}
                    overlay={renderPlayInfo}
                  >
                    <IconButton
                      onClick={
                        wave
                          ? playing
                            ? () => wave.pause()
                            : () => wave.play()
                          : null
                      }
                      size="small"
                    >
                      {playing ? (
                        <PauseIcon fontSize="medium" />
                      ) : (
                        <PlayArrowIcon fontSize="medium" />
                      )}
                    </IconButton>
                  </OverlayTrigger>
                </Box>
                <Box>
                  <OverlayTrigger
                    placement="bottom"
                    delay={{ show: 100, hide: 100 }}
                    overlay={renderRecordInfo}
                  >
                    <IconButton size="small" onClick={start}>
                      <MicNoneIcon fontSize="medium" />
                    </IconButton>
                  </OverlayTrigger>
                </Box>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  flexWrap: "wrap",
                  justifyContent: "right",
                }}
              >
                {currentProject && currentProject.files.media.length ? (
                  <Box
                    sx={{
                      p: 1,
                      alignItems: "end",
                    }}
                  >
                    <Button
                      variant="outlined"
                      color="primary"
                      onClick={() => setTranscribeData(true)}
                      disabled={currentProject.transcribed ? true : false}
                    >
                      {transcribing ? (
                        <CircularProgress size="20px" color="inherit" />
                      ) : currentProject.transcribed ? (
                        <DoneIcon fontSize="small" color="inherit" />
                      ) : null}{" "}
                      {currentProject.transcribed
                        ? "Transcribed"
                        : transcribing
                        ? "Transcribing"
                        : "Transcribe"}
                    </Button>
                  </Box>
                ) : null}
                <Box>
                  <OverlayTrigger
                    placement="bottom"
                    delay={{ show: 100, hide: 100 }}
                    overlay={renderDeleteInfo}
                  >
                    <IconButton onClick={handleDelete} size="small">
                      <DeleteIcon fontSize="large" color="error" />
                    </IconButton>
                  </OverlayTrigger>
                </Box>
              </Box>
            </Box>
          </Card.Text>
        </Container>
        <Container className="card-container">
          {currentProject && currentProject.files.media.length ? (
            <>
              <Box sx={{ p: 1 }}>
                <Card variant="center" style={{ width: 900, height: 520 }}>
                  {/* <WaveSurfer currentProject={currentProject} /> */}
                  <TranscriptionWrapper
                    isTranscribed={isTranscribed}
                    setCurrentProject={setCurrentProject}
                    currentProject={currentProject}
                    setPlaying={setPlaying}
                    playing={playing}
                    playBack={playBack}
                    setWave={setWave}
                    transcriptData={transcriptWords}
                    setPlayBack={setPlayBack}
                    user={user}
                  />
                </Card>
              </Box>
            </>
          ) : (
            <>
              <Box
                sx={{
                  p: 1,
                  display: "flex",
                  flexDirection: "column",
                  flexWrap: "wrap",
                  justifyContent: "start",
                }}
              >
                {podcasting ? (
                  <Box>
                    <AudioRecordingView
                      isRecording={isRecording}
                      setUploadStatus={setUploadStatus}
                      uploadStatus={uploadStatus}
                      isTranscripting={isTranscripting}
                      setIsTranscripting={setIsTranscripting}
                      currentProject={currentProject}
                      setCurrentProject={setCurrentProject}
                    />
                  </Box>
                ) : (
                  <Card className="col-md-5 mx-auto upload-file-container">
                    <Card.Body
                      className="mx-auto"
                      onClick={() => setShowOptionModal(true)}
                    >
                      <Box
                        sx={{
                          p: 1,
                          display: "flex",
                          flexDirection: "column",
                          flexWrap: "wrap",
                          justifyContent: "center",
                        }}
                      >
                        <Box sx={{ p: 1, paddingBottom: "5px" }}>
                          <InsertDriveFileIcon
                            style={{
                              fontSize: "100px",
                            }}
                            color="disabled"
                          />
                        </Box>

                        <Box sx={{ p: 1 }}>
                          <AddIcon color="action" fontSize="large" />
                          <Typography variant="h6" style={{ color: "gray" }}>
                            Click to add audio or video File.
                          </Typography>
                        </Box>
                      </Box>
                    </Card.Body>
                  </Card>
                )}
              </Box>
            </>
          )}
        </Container>
      </Box>

      <ShowOptions
        show={showOptionModal}
        user={user}
        setCurrentProject={setCurrentProject}
        setRecordId={setRecordId}
        setLocalFile={setLocalFile}
        localFile={localFile}
        setIsWorkSpaceFile={setIsWorkSpaceFile}
        currentProject={currentProject}
        setShowOptions={setShowOptionModal}
        // setRecordingMedia={setRecordingMedia}
      />

      <AudioRecordingModal
        show={isRecording}
        setBlobFile={setBlobFile}
        setUploadStatus={setUploadStatus}
        setIsRecording={setIsRecording}
      />
    </>
  );
}

export default DataTranscription;
