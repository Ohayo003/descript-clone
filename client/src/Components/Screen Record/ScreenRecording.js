import React, { useState, useEffect } from "react";
import { DropdownButton, Dropdown, Modal, Card } from "react-bootstrap";
import { useReactMediaRecorder } from "react-media-recorder";
// import StopIcon from "@mui/icons-material/Stop";
import { Container } from "react-floating-action-button";
import axios from "../../axios";
import { toast } from "react-toastify";
import { options } from "../../ToastifyOptions";
import Box from "@mui/material/Box";
import Button from "@material-ui/core/Button";
import PersonalVideoIcon from "@mui/icons-material/PersonalVideo";
import VideocamIcon from "@mui/icons-material/Videocam";
import { IconButton } from "@material-ui/core";
import "./ScreenRecording.css";
import MaleAvatar from "../../assets/images/img_avatar_male.png";
import FemaleAvatar from "../../assets/images/img_avatar_female.png";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import CloseIcon from "@mui/icons-material/Close";
import PauseIcon from "@mui/icons-material/Pause";
import DoneIcon from "@mui/icons-material/Done";

const ScreenRecording = ({
  user,
  show,
  setShowRecordModal,
  setShowCanvas,
  showControls,
  setShowControls,
  setTime,
  time,
}) => {
  const [screenRecord, setScreenRecord] = useState(false);
  const [cameraRecord, setCameraRecord] = useState(false);
  const [timerStart, setTimerStart] = useState(false);
  const [done, setDone] = useState(false);
  // const [media, setMedia] = useState(null);
  const [Title, setTitle] = useState("Choose a Screen to share");
  const {
    startRecording,
    stopRecording,
    pauseRecording,
    resumeRecording,
    previewStream,
    mediaBlobUrl,
    status,
  } = useReactMediaRecorder({
    video: cameraRecord,
    screen: screenRecord,
  });

  const handleCancelRecording = (e) => {
    e.preventDefault();
    stopRecording();
    handleCloseModal();
  };

  useEffect(() => {
    if (timerStart) {
      const timer = setTimeout(() => {
        const { minutes, seconds } = time;

        if (seconds > 0) {
          setTime({ minutes: minutes, seconds: seconds - 1 });
        }

        if (seconds === 0) {
          if (minutes === 0) {
            clearTimeout(timer);
            setTimerStart(false);
            setTime({ minutes: 5, seconds: 0 });
          } else {
            setTime({ minutes: minutes - 1, seconds: 59 });
          }
        }
      }, 1000);
    }
  }, [time, timerStart, setTime]);

  const handleCloseModal = () => {
    setShowCanvas(false);
    setShowRecordModal(false);
    setShowControls(false);
  };

  //sets the media to screen only
  const screenOnly = (e) => {
    setScreenRecord(true);
    setCameraRecord(false);
  };
  //sets the media device to camera only
  const cameraOnly = (e) => {
    setCameraRecord(true);
    setScreenRecord(false);
  };

  //Sets the title name of the dropdown button on switch
  const handleSwitchTitle = (e) => {
    //TODO: add both camera and Screen recording at the same time
    switch (e) {
      // case "1":
      //   setTitle("Screen and Camera");
      //   break;
      case "1":
        setTitle("Screen Only");
        break;
      case "2":
        setTitle("Camera Only");
        break;
      default:
        setTitle("Choose a Screen to share");
    }
  };

  // console.log(optionRecord);
  useEffect(() => {
    if (done && mediaBlobUrl && status === "stopped") {
      let blobUrl = mediaBlobUrl;
      const upload_recording = async () => {
        console.log(mediaBlobUrl);
        let mediaFile;
        var formData = new FormData();
        // let media;
        try {
          await fetch(blobUrl)
            .then((res) => res.blob())
            .then((file) => {
              return (mediaFile = file);
            });
          console.log(mediaFile);
          // console.log(media);
          if (mediaFile.type === "video/mp4" || "audio/wav") {
            formData.append("Recordings", mediaFile);
          } else {
            setDone(false);
          }
          formData.append("projectName", "Recordings");

          let config = {
            headers: {
              "Content-type": "multipart/form-data",
            },
          };
          const result = axios.put(
            `/projects/file-upload/video-recordings/${user._id}`,
            formData,
            config
          );
          await toast
            .promise(
              result,
              {
                pending: `Saving video. Please wait.`,
                success: `video successfully uploaded.`,
                error: `Something went wrong while uploading video`,
              },
              options
            )
            .then(() => {
              setTime({ minutes: 5, seconds: 0 });
              setDone(false);
              blobUrl = null;
              // blob = null;
            })
            .catch((error) => {
              console.error(error);
              setDone(false);
            });
          setDone(false);
        } catch (error) {
          console.error(error);
        }
      };
      upload_recording();
      setDone(false);
    }
  }, [done, mediaBlobUrl, setTime, status, user]);

  const start = () => {
    startRecording();
    setTimerStart(true);
    // setShowRecordModal(false);
  };

  const stopRecord = async () => {
    setDone(true);
    stopRecording();
    console.log(mediaBlobUrl);
    // upload_recording(mediaBlobUrl);
    setTimerStart(false);
    setShowRecordModal(false);

    setTime({ minutes: 5, seconds: 0 });
  };

  return (
    <>
      <Modal
        size="sm"
        show={show}
        onHide={handleCloseModal}
        className="record-video-modal"
        backdrop={false}
        animation={true}
      >
        <Modal.Title closButton>
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              flexWrap: "wrap",
              width: "100%",
              justifyContent: "space-between",
            }}
          >
            <Box sx={{ p: 2 }}>Descript Record</Box>
            <Box sx={{ p: 2 }}>
              <IconButton size="small" onClick={handleCloseModal}>
                <CloseIcon fontSize="small" />
              </IconButton>
            </Box>
          </Box>
        </Modal.Title>
        <Modal.Body>
          <Box
            sx={{
              display: "flex",
              flexWrap: "wrap",
              flexDirection: "column",
              alignContent: "start",
            }}
          >
            <Box sx={{ width: "100%", flexGrow: 1 }}>
              <DropdownButton
                align="start"
                id="dropdown-menu-align-end"
                variant="light"
                title={Title}
                style={{ display: "block", width: "100%" }}
                onSelect={(eventKey) => handleSwitchTitle(eventKey)}
              >
                <Dropdown.Item eventKey="1" onClick={(e) => screenOnly(e)}>
                  <PersonalVideoIcon /> Screen Only
                </Dropdown.Item>
                <Dropdown.Item eventKey="2" onClick={(e) => cameraOnly(e)}>
                  <VideocamIcon /> Camera Only
                </Dropdown.Item>
              </DropdownButton>
            </Box>
            <br />
            <Box sx={{ width: "100%" }}>
              <Button
                style={{ width: "100%" }}
                variant="contained"
                color="primary"
                onClick={start}
              >
                <PlayArrowIcon /> Start Recording
              </Button>
            </Box>
          </Box>
        </Modal.Body>
        {/* Show Controls for Recording */}
        {showControls ? (
          <Container className="floating-Controls">
            <Container className="button-container">
              <IconButton onClick={handleCancelRecording}>
                <CloseIcon fontSize="large" color="error" />
              </IconButton>
              <IconButton
                onClick={status === "paused" ? resumeRecording : pauseRecording}
                disabled={status !== "recording" ? true : false}
              >
                {status === "paused" ? (
                  <PlayArrowIcon fontSize="large" color="primary" />
                ) : (
                  <PauseIcon fontSize="large" color="error" />
                )}
              </IconButton>
              <IconButton onClick={status === "recording" ? stopRecord : start}>
                {status === "recording" && "paused" ? (
                  <DoneIcon fontSize="large" color="primary" />
                ) : (
                  <PlayArrowIcon fontSize="large" color="primary" />
                )}
              </IconButton>
              <Card className="duration-container">
                <h6>
                  {time.minutes === 0 && time.seconds === 0 ? (
                    stopRecord()
                  ) : (
                    <h6>
                      {time.minutes < 10 ? `0${time.minutes}` : time.minutes} :{" "}
                      {time.seconds < 10 ? `0${time.seconds}` : time.seconds}
                    </h6>
                  )}
                </h6>
              </Card>
            </Container>
            <div className="user-image-video-Container">
              {cameraRecord ? (
                <video src={previewStream} className="user-video" />
              ) : (
                // <VideoPreview stream={previewStream} className="user-video" />
                <img
                  className="user-image-avatar"
                  src={user.gender === "Male" ? MaleAvatar : FemaleAvatar}
                  alt="user Avatar"
                />
              )}
            </div>
          </Container>
        ) : null}
      </Modal>
    </>
  );
};

export default ScreenRecording;
