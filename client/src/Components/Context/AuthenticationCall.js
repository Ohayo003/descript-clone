import { toast } from "react-toastify";
import axios from "../../axios";
import { options } from "../../ToastifyOptions";
import {
  SIGNING_IN,
  SIGNING_OUT,
  SIGNING_UP,
  SIGNIN_FAILED,
  SIGNIN_SUCCESS,
  SIGNUP_FAILED,
  SIGNUP_SUCCESS,
  SIGN_OUT,
} from "./ActionContext";

export const sign_in = async (userCred, dispatch) => {
  dispatch({ type: SIGNING_IN });
  try {
    console.log("Hello");
    const user = await axios.post(`/users/login`, userCred);
    console.log(user.data);
    toast.success("You are Loggedin successfully", options);
    const { password, createdAt, updatedAt, projectFiles, ...others } =
      user.data;
    dispatch({ type: SIGNIN_SUCCESS, payload: others });
  } catch (err) {
    console.log(err);
    toast.error("Invalid Email or Password", options);
    dispatch({ type: SIGNIN_FAILED, payload: err });
  }
};
export const sign_up = async (
  userCred,
  folder_recording,
  audio_recording,
  dispatch
) => {
  dispatch({ type: SIGNING_UP });
  try {
    const user = await axios.post("/users/create-account", userCred);
    toast.success(`Account ${user.data.email} has been created succuessfuly!`);
    const create_audio_folder = await axios.post(
      "/train-audio/create/audio-training-folder",
      audio_recording
    );
    const { password, createdAt, updatedAt, projectFiles, ...others } =
      user.data;
    if (create_audio_folder) {
      await axios.post("/projects/create-project-folder", folder_recording);
    }
    dispatch({ type: SIGNUP_SUCCESS, payload: others });
  } catch (error) {
    toast.error("Email already Exist!", options);
    dispatch({ type: SIGNUP_FAILED, payload: error });
  }
};

export const sign_out = async (dispatch) => {
  dispatch({ type: SIGNING_OUT });

  localStorage.removeItem("credentials");
  localStorage.removeItem("Authorized");
  await dispatch({ type: SIGN_OUT });
};
