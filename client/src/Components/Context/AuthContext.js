import Reducer from "./Reducer";
import { createContext, useReducer, useEffect } from "react";
// import {encryptStorage} from '../../utils'

const INITIAL_STATE = {
  user: JSON.parse(localStorage.getItem("credentials")) || null,
  isFetching: false,
  isAuthenticated: localStorage.getItem("Authorized") || false,
  error: false,
};

export const AuthContext = createContext(INITIAL_STATE);

export const AuthProvider = ({ children }) => {
  const [state, dispatch] = useReducer(Reducer, INITIAL_STATE);

  useEffect(() => {
    localStorage.setItem("credentials", JSON.stringify(state.user));
    // encryptStorage.setItem('credentials', state.user)
    localStorage.setItem("Authorized", state.isAuthenticated);
  }, [state.user, state.isAuthenticated]);

  return (
    <AuthContext.Provider
      value={{
        user: state.user,
        isFetching: state.isFetching,
        isAuthenticated: state.isAuthenticated,
        error: state.error,
        dispatch,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
