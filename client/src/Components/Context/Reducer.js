import {
  SIGNING_IN,
  SIGNING_OUT,
  SIGNING_UP,
  SIGNIN_FAILED,
  SIGNIN_SUCCESS,
  SIGNUP_FAILED,
  SIGNUP_SUCCESS,
  SIGN_OUT,
} from "./ActionContext";

const Reducer = (state, action) => {
  switch (action.type) {
    case SIGNING_IN:
      return {
        user: null,
        isFetching: true,
        isAuthenticated: false,
        error: false,
      };
    case SIGNIN_SUCCESS:
      return {
        user: action.payload,
        isFetching: false,
        isAuthenticated: true,
        error: false,
      };
    case SIGNIN_FAILED:
      return {
        user: null,
        isFetching: false,
        isAuthenticated: false,
        error: action.payload,
      };
    case SIGNING_UP:
      return {
        user: null,
        isFetching: true,
        isAuthenticated: false,
        error: false,
      };
    case SIGNUP_SUCCESS:
      return {
        user: action.payload,
        isAuthenticated: true,
        isFetching: false,
        error: false,
      };
    case SIGNUP_FAILED:
      return {
        user: null,
        isFetching: false,
        isAuthenticated: false,
        error: action.payload,
      };
    case SIGN_OUT:
      return {
        user: null,
        isFetching: false,
        isAuthenticated: false,
        error: false,
      };
    case SIGNING_OUT:
      return {
        user: null,
        isFetching: true,
        isAuthenticated: false,
        error: false,
      };
    default:
      return state;
  }
};

export default Reducer;
