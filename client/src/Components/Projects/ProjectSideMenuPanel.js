import React from "react";
import {
  ProSidebar,
  Menu,
  SidebarHeader,
  MenuItem,
  SubMenu,
  SidebarContent,
  SidebarFooter,
} from "react-pro-sidebar";
import "react-pro-sidebar/dist/css/styles.css";
import FolderIcon from "@material-ui/icons/Folder";
import { Badge } from "react-bootstrap";
import axios from "../../axios";
import Typography from "@mui/material/Typography";
import "./ProjectSideMenuPanel.css";

function ProjectSideMenuPanel({
  setProjectID,
  projects,
  setTitleProject,
  setRecordingID,
  setMyProjects,
}) {
  // const [projectID, setProjectID] = useState();
  const handleResetRecordingID = () => {
    return setRecordingID("");
  };
  const handleResetProjectID = () => {
    return setProjectID("");
  };

  const handleRedirectToMyProjects = () => {
    setMyProjects(true);
    handleResetProjectID();
    handleResetRecordingID();
  };

  const getProjectID = async (id) => {
    var _id = `${id}`;
    try {
      await axios
        .get(`/projects/get-project/${_id}`)
        .then((result) => {
          console.log(result);
          if (result.data.projectName === "Recordings") {
            setTitleProject(result.data.projectName);
            setMyProjects(false);
            handleResetProjectID();
            return setRecordingID(result.data._id);
          } else {
            setTitleProject(result.data.projectName);
            setMyProjects(false);
            handleResetRecordingID();
            return setProjectID(result.data._id);
          }
        })
        .catch((error) => console.error(error));
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <ProSidebar toggled={true} className="side-bar-menu">
      <SidebarHeader
        style={{ padding: "5px", textAlign: "center" }}
        onClick={handleRedirectToMyProjects}
      >
        <Typography variant="h4" className="Project-redirect">
          Project Files
        </Typography>
      </SidebarHeader>
      <SidebarContent>
        <Menu>
          <SubMenu
            title="Projects"
            icon={<FolderIcon />}
            suffix={<Badge bg="info">{projects ? projects.length : 0}</Badge>}
          >
            {projects
              ? projects.map((project) => (
                  <MenuItem
                    prefix={<FolderIcon />}
                    // onClick={() => get_project_files()}

                    key={project._id}
                    onMouseDown={() => getProjectID(project._id)}
                  >
                    {project.projectName}
                  </MenuItem>
                ))
              : "There are currently no projects"}
          </SubMenu>
        </Menu>
      </SidebarContent>
      <SidebarFooter style={{ textAlign: "center", padding: "10px" }}>
        Hello side bar
      </SidebarFooter>
    </ProSidebar>
  );
}
export default ProjectSideMenuPanel;
