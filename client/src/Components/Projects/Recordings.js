import React, { useEffect, useState } from "react";
import Box from "@material-ui/core/Box";
import { Card, Container } from "react-bootstrap";
import MaterialCard from "@mui/material/Card";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import moment from "moment";
import axios from "../../axios";
import { toast } from "react-toastify";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { IconButton } from "@material-ui/core";
import PlayRecordingModal from "../Modals/PlayRecordingModal";

function Recordings({
  recordingID,
  setRecordingID,
  titleProject,
  setMyProjects,
}) {
  const [videoRecordings, setVideoRecordings] = useState(null);
  const [playRecording, setPlayRecording] = useState(null);
  const [show, setShow] = useState(false);

  useEffect(() => {
    async function fetchdata() {
      const getRecordings = axios.get(`/projects/get-project/${recordingID}`);
      await toast
        .promise(getRecordings, {
          pending: "Fetching Video Recordings...",
          success: "Successfully Fetched Videos",
          error: "Failed while Fetching Video Recordings...",
        })
        .then((result) => {
          console.log(result.data);
          return setVideoRecordings(result.data);
        })
        .catch((error) => console.log(error));
    }
    fetchdata();
  }, [recordingID]);

  const handleBack = () => {
    setMyProjects(true);
    setRecordingID("");
  };

  return (
    <>
      <Card.Title>
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            flexDirection: "row",
            alignContent: "start",
            bgcolor: "background.paper",
            width: "100%",
          }}
          className="card-title-flexbox"
        >
          <Box
            sx={{
              p: 1,
              m: 1,
              display: "flex",
              flexWrap: "wrap",
              flexDirection: "row",
              alignContent: "start",
              flexGrow: 1,
            }}
            className="card-title-flexbox-item-title"
          >
            {" "}
            <Box sx={{ paddingRight: "5px" }}>
              <Breadcrumbs>
                <IconButton size="small" onClick={handleBack}>
                  <ArrowBackIcon fontSize="medium" />
                </IconButton>
              </Breadcrumbs>
            </Box>
            <Box>
              <Breadcrumbs>
                <Typography variant="h4">My Projects</Typography>
              </Breadcrumbs>
            </Box>
            {titleProject ? (
              <>
                <Box>
                  <Breadcrumbs>
                    <NavigateNextIcon fontSize="small" />
                  </Breadcrumbs>
                </Box>
                <Box>
                  <Breadcrumbs>
                    <Typography variant="h4">{titleProject}</Typography>
                  </Breadcrumbs>
                </Box>
              </>
            ) : null}
          </Box>
          {/* <Box sx={{ p: 1, m: 1 }} className="card-title-flexbox-item">
            <Button
              variant="outlined"
              color="primary"
              //   onClick={handleShowAddModal}
            >
              <AddIcon /> new Project
            </Button>
          </Box> */}
        </Box>
      </Card.Title>
      <Container>
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            flexDirection: "row",
            alignContent: "start",
            p: 1,
            m: 1,
            bgcolor: "background.paper",
            maxWidth: 1000,
          }}
        >
          {/* {alert(recordingID)} */}
          {videoRecordings && videoRecordings.files.media.length ? (
            videoRecordings.files.media.map((videos, index) => (
              <Box sx={{ p: 1 }} key={index}>
                <MaterialCard
                  border="secondary"
                  className="workspace-drive-videos"
                  onMouseDown={() => {
                    setPlayRecording(videos);
                    setShow(true);
                  }}
                >
                  <CardMedia
                    className="preview-container"
                    component="video"
                    alt="Recorded Video"
                    src={videos.url}
                  />

                  <CardContent>
                    <Typography gutterBottom variant="caption">
                      {videos.name} -{" "}
                      {moment(videos.createdAt).format("YYYY-MM-DD")}
                    </Typography>
                  </CardContent>
                </MaterialCard>
              </Box>
            ))
          ) : (
            <Typography variant="h5" className="h5-no-video">
              There are currently no Recording Video's in the Workspace
            </Typography>
          )}
        </Box>
      </Container>

      <PlayRecordingModal
        show={show}
        setShow={setShow}
        playRecording={playRecording}
      />
    </>
  );
}

export default Recordings;
