import React, { useState } from "react";
import { Card } from "react-bootstrap";
import AddIcon from "@mui/icons-material/Add";
import Button from "@material-ui/core/Button";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import AddProjectModal from "../Modals/AddProjectModal";
import moment from "moment";
import axios from "../../axios";
import Box from "@mui/material/Box";
import "./Workspace.css";
import Typography from "@mui/material/Typography";

function Workspace({
  user,
  projects,
  setProjectID,
  titleProject,
  setRecordingID,
  setMyProjects,
  setTitleProject,
}) {
  const [showAddModal, setShowAddModal] = useState(false);
  const handleShowAddModal = () => setShowAddModal(true);

  const checkProjectId = async (id) => {
    var _id = `${id}`;
    await axios
      .get(`/projects/get-project/${_id}`)
      .then((result) => {
        if (result.data.projectName === "Recordings") {
          console.log(result.data);
          setMyProjects(false);
          setTitleProject(result.data.projectName);
          return setRecordingID(result.data._id);
        } else {
          setMyProjects(false);

          // localStorage.setItem(
          //   "currentProject",
          //   JSON.stringify(result.data._id)
          // );
          return setProjectID(result.data._id);
        }
      })
      .catch((error) => console.error(error));
  };

  return (
    <>
      <Card.Title>
        <Box
          sx={{
            display: "flex",
            flexWrap: "wrap",
            flexDirection: "row",
            alignContent: "start",
            bgcolor: "background.paper",
            width: "100%",
          }}
          className="card-title-flexbox"
        >
          <Box
            sx={{
              p: 1,
              m: 1,
              display: "flex",
              flexWrap: "wrap",
              flexDirection: "row",
              alignContent: "start",
              flexGrow: 1,
            }}
            className="card-title-flexbox-item-title"
          >
            <Box>
              <Breadcrumbs>
                <Typography variant="h4">My Projects</Typography>
              </Breadcrumbs>
            </Box>
            {titleProject ? (
              <>
                <Box>
                  <Breadcrumbs>
                    <NavigateNextIcon fontSize="small" />
                  </Breadcrumbs>
                </Box>
                <Box>
                  <Breadcrumbs>
                    <Typography variant="h4">{titleProject}</Typography>
                  </Breadcrumbs>
                </Box>
              </>
            ) : null}
          </Box>
          <Box sx={{ p: 1, m: 1 }} className="card-title-flexbox-item">
            <Button
              variant="outlined"
              color="primary"
              onClick={handleShowAddModal}
            >
              <AddIcon /> new Project
            </Button>
          </Box>
        </Box>
      </Card.Title>
      <Card.Body className="card-table-body">
        <Paper sx={{ overflow: "hidden" }} className="paper-table-container">
          <TableContainer
            sx={{ maxHeight: 520 }}
            className="project-table-container"
          >
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  <TableCell align="left" colSpan={1}>
                    Name
                  </TableCell>
                  <TableCell align="center" colSpan={1}>
                    Owner
                  </TableCell>
                  <TableCell align="center" colSpan={1}>
                    Created
                  </TableCell>
                  <TableCell align="center" colSpan={1}>
                    Last opened <ArrowDownwardIcon fontSize="small" />
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {projects.map((project) => (
                  <TableRow
                    className="data-row-header"
                    hover
                    key={project._id}
                    onMouseDown={() => checkProjectId(project._id)}
                  >
                    <TableCell align="left" colSpan={1}>
                      {project.projectName}
                    </TableCell>
                    <TableCell align="center" colSpan={1}>
                      {project.owner === user.email ? "me" : "owned by other"}
                    </TableCell>
                    <TableCell align="center" colSpan={1}>
                      {moment(project.createdAt).format("YYYY-MM-DD")}
                    </TableCell>
                    <TableCell align="center" colSpan={1}>
                      {moment(project.updateAt).format("DD-MM-YYYY")}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </Card.Body>

      <AddProjectModal
        showAddModal={showAddModal}
        setShowAddModal={setShowAddModal}
        owner={user.email}
        setMyProjects={setMyProjects}
        setProjectID={setProjectID}
      />
    </>
  );
}

export default Workspace;
