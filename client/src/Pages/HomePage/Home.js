import React, { useEffect, useState } from "react";
import Header from "../../Components/Header/Header";
import ScreenRecording from "../../Components/Screen Record/ScreenRecording";
import ProjectSideMenuPanel from "../../Components/Projects/ProjectSideMenuPanel";
import { Card } from "react-bootstrap";
import Workspace from "../../Components/Projects/Workspace";
// import Podcasting from "../../Components/Podcasting/Podcasting";
import "./Home.css";
import axios from "../../axios";
import Box from "@mui/material/Box";
import { toast } from "react-toastify";
import Recordings from "../../Components/Projects/Recordings";
import DataTranscription from "../../Components/Transcription/DataTranscription";

function Home({ user }) {
  const [myProjects, setMyProjects] = useState(true);
  // const [podcastingLink, setPodcastingLink] = useState(false);
  const [projects, setProjects] = useState([]);
  // const [currentProject, setCurrentProject] = useState(null);
  const [titleProject, setTitleProject] = useState();
  const [showRecordModal, setShowRecordModal] = useState(false);
  const [showCanvas, setShowCanvas] = useState(false);
  const [showControls, setShowControls] = useState(false);
  const [recordingID, setRecordingID] = useState("");
  const [projectID, setProjectID] = useState("");
  const [time, setTime] = useState({
    minutes: 5,
    seconds: 0,
  });
  // Load projects of user from the database
  useEffect(() => {
    try {
      // let data = {
      //   userID: user._id,
      // };
      axios
        .get(`/projects/user-projects/${user._id}`)
        .then((result) => {
          if (result) {
            // console.log(result);
            setProjects(result.data);
          } else {
            toast.error("Something went wrong while fetching project data");
          }
        })
        .catch((error) => console.error(error));
    } catch (error) {
      console.log(error);
    }
  }, [projects, user._id]);

  // useEffect(() => {
  //   setProjectID(JSON.parse(localStorage.getItem("currentProject")));
  // }, []);

  return (
    <>
      <Header
        user={user}
        setProjectID={setProjectID}
        setRecordingID={setRecordingID}
        setMyProjectsLink={setMyProjects}
        // setPodcastingLink={setPodcastingLink}
        setShowRecordModal={setShowRecordModal}
        setShowCanvas={setShowCanvas}
        setTitleProject={setTitleProject}
        setShowControls={setShowControls}
        setTime={setTime}
      />
      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          flexDirection: "row",
          alignContent: "start",
          p: 1,
          m: 1,
          bgcolor: "background.paper",
          maxWidth: "100%",
        }}
      >
        <Box sx={{ p: 1 }}>
          <Card style={{ height: "50rem" }} className="side-bar-panel">
            {projects.length ? (
              <ProjectSideMenuPanel
                projects={projects}
                setTitleProject={setTitleProject}
                setProjectID={setProjectID}
                setMyProjects={setMyProjects}
                setRecordingID={setRecordingID}
              />
            ) : (
              <ProjectSideMenuPanel projects={0} />
            )}
          </Card>
        </Box>
        <Box sx={{ p: 1 }}>
          <Card className="card-home-body">
            <Card.Body>
              <>
                {myProjects && (
                  <Workspace
                    user={user}
                    projects={projects}
                    titleProject={titleProject}
                    setProjectID={setProjectID}
                    setRecordingID={setRecordingID}
                    setMyProjects={setMyProjects}
                    setTitleProject={setTitleProject}
                  />
                )}
                {projectID && (
                  <DataTranscription
                    projectID={projectID}
                    setProjectID={setProjectID}
                    setMyProjects={setMyProjects}
                  />
                )}
                {recordingID && (
                  <Recordings
                    recordingID={recordingID}
                    setRecordingID={setRecordingID}
                    titleProject={titleProject}
                    setMyProjects={setMyProjects}
                  />
                )}
              </>
            </Card.Body>
          </Card>
        </Box>
      </Box>
      <ScreenRecording
        show={showRecordModal}
        user={user}
        showCanvas={showCanvas}
        setShowCanvas={setShowCanvas}
        setShowRecordModal={setShowRecordModal}
        showControls={showControls}
        setShowControls={setShowControls}
        setTime={setTime}
        time={time}
      />
    </>
  );
}

export default Home;
