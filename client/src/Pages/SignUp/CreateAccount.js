import React, { useContext, useRef, useState } from "react";
import descriptLogo from "../../assets/images/descript_logo.png";
import { Route, Link } from "react-router-dom";
import { CircularProgress } from "@material-ui/core";
import { AuthContext } from "../../Components/Context/AuthContext";
import "./CreateAccount.css";
import Login from "../Login/Login";
import { Form } from "react-bootstrap";
import { sign_up } from "../../Components/Context/AuthenticationCall";
import { toast } from "react-toastify";
import { options } from "../../ToastifyOptions";

function CreateAccount() {
  const { isFetching, dispatch } = useContext(AuthContext);
  const firstName = useRef();
  const lastName = useRef();
  const email = useRef();
  const password = useRef();
  const [gender, setGender] = useState("");

  const createAccount = async (e) => {
    e.preventDefault();
    const reg_firstName = firstName.current.value;
    const reg_lastName = lastName.current.value;
    const reg_email = email.current.value;
    const reg_password = password.current.value;
    const reg_gender = gender;

    if (reg_firstName && reg_lastName && reg_email && reg_password) {
      const folder = {
        record: {
          projectName: "Recordings",
          email: reg_email,
        },
        audio_trainings: { projectName: "Trained_Audio", email: reg_email },
      };
      await sign_up(
        {
          firstName: reg_firstName,
          lastName: reg_lastName,
          gender: reg_gender,
          email: reg_email,
          password: reg_password,
        },
        folder.record,
        folder.audio_trainings,
        dispatch
      );
    } else {
      toast.error("Please provide all the needed details.", options);
    }
  };

  return (
    <div className="Signup-container">
      <div className="card container-fluid" id="card">
        <div className="flexboxContainer mt-5 mb-5 ms-5 me-5">
          <div className="flexbox-item">
            <div className="container pt-3 reg-form-container">
              <h1 className="create-account">Create Account</h1>
              <form className="form-group" onSubmit={createAccount}>
                <label htmlFor="firstName" className="reg-firstName-label">
                  First Name
                </label>
                <input
                  className="form-control reg-firstName-input"
                  type="text"
                  name="firstName"
                  ref={firstName}
                  placeholder="Enter First Name"
                />
                <label htmlFor="lastName" className="reg-firstName-label">
                  Last Name
                </label>
                <input
                  className="form-control reg-lastName-input"
                  type="text"
                  name="lastName"
                  ref={lastName}
                  placeholder="Enter Last Name"
                />
                <label htmlFor="gender" className="reg-gender-label">
                  Gender:
                </label>
                <Form.Select onClick={(e) => setGender(e.target.value)}>
                  <option style={{ color: "lightgray" }}>
                    Select Gender...
                  </option>
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                </Form.Select>
                <label htmlFor="reg-email" className="reg-email-label">
                  Email Address
                </label>
                <input
                  className="form-control reg-email-input"
                  type="email"
                  name="reg-email"
                  ref={email}
                  placeholder="Enter Email"
                />
                <label htmlFor="reg-password" className="reg-password-label">
                  Password
                </label>
                <input
                  className="form-control reg-password-input"
                  type="password"
                  name="reg-password"
                  ref={password}
                  placeholder="Enter Password..."
                />
                <div className="login-link">
                  <label>already have an account ?&nbsp;</label>
                  <Link to="/">
                    <label className="login">Login</label>
                  </Link>
                </div>
                <br />
                <button
                  className="btn btn-outline-light btn-create-account"
                  type="submit"
                >
                  {isFetching ? (
                    <CircularProgress color={"primary"} size="20px" />
                  ) : (
                    "Create Account"
                  )}
                </button>
              </form>
            </div>
          </div>
          <div className="flexbox-item">
            <div className="container right-container">
              <img src={descriptLogo} alt="Descript Logo" className="logo" />
              <h3>Descript-clone App</h3>
              <p>An All-in-one auido & video editing as easy as doc.</p>
              <p>Created for users that are new to media editor.</p>
            </div>
          </div>
        </div>
        <Route exact path="/" component={Login}></Route>
      </div>
    </div>
  );
}

export default CreateAccount;
