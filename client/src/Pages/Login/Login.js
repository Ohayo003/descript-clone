import React, { useRef, useContext } from "react";
import { AuthContext } from "../../Components/Context/AuthContext";
import "./Login.css";
import descriptLogo from "../../assets/images/descript_logo.png";
import { Link, Route } from "react-router-dom";
import { CircularProgress } from "@material-ui/core";
import { sign_in } from "../../Components/Context/AuthenticationCall";
import { toast } from "react-toastify";
import { options } from "../../ToastifyOptions";
import CreateAccount from "../SignUp/CreateAccount";

function Login() {
  const { dispatch, isFetching } = useContext(AuthContext);
  const email = useRef();
  const password = useRef();

  const login = async (e) => {
    const emailVal = email.current.value;
    const passwordVal = password.current.value;
    e.preventDefault();
    if (emailVal && passwordVal) {
      await sign_in({ email: emailVal, password: passwordVal }, dispatch);
    } else {
      toast.error("Please Provide Email and Password", options);
    }
  };

  return (
    <div className="login-container">
      <div className="card container-fluid" id="card">
        <div className="flexboxContainer mt-5 mb-5 ms-5 me-5">
          <div className="flexbox-item">
            <div className="container pt-3 form-container">
              <h1 className="Login">Login</h1>
              <form className="form-group" onSubmit={login}>
                <label htmlFor="email" className="email-label">
                  Email Address
                </label>
                <input
                  className="form-control email-input"
                  type="email"
                  ref={email}
                  placeholder="Enter Email"
                />
                <label htmlFor="password" className="password-label">
                  Password
                </label>
                <input
                  className="form-control password-input"
                  type="password"
                  ref={password}
                  placeholder="Enter Password..."
                />
                <div className="sign-up-link">
                  <label>don't have an account yet ?&nbsp;</label>
                  <Link to="/create-account">
                    <label className="sign-up">Create Account</label>
                  </Link>
                </div>
                <br />
                <button
                  className="btn btn-outline-light btn-login"
                  type="submit"
                >
                  {isFetching ? (
                    <CircularProgress color={"primary"} size="20px" />
                  ) : (
                    "Login"
                  )}
                </button>
              </form>
            </div>
          </div>
          <div className="flexbox-item">
            <div className="container right-container">
              <img src={descriptLogo} alt="Descript Logo" className="logo" />
              <h3>Descript-clone App</h3>
              <p>An All-in-one auido & video editing as easy as doc.</p>
              <p>Created for users that are new to media editor.</p>
            </div>
          </div>
        </div>
        <Route path="/create-account" component={CreateAccount} />
      </div>
    </div>
  );
}

export default Login;
