import Amplify from 'aws-amplify'
import {EncryptStorage} from 'encrypt-storage';

export const encryptStorage = EncryptStorage('long-secret-is-better',{
    storageType:'localStorage',
    stateManagementUse: true,
    prefix: '@prefix'

})

Amplify.configure({
    Auth: {
        storage: encryptStorage,
    }
})