import axios from "axios";

const instance = axios.create({
  // baseURL: "http://localhost:3000/api",
  baseURL: "/server/v1",
  // baseURL: "https://167.172.228.148/api",
});

export default instance;
