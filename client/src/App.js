import "./App.css";
import Login from "./Pages/Login/Login";
import {
  Switch,
  Route,
  BrowserRouter as Router,
  Redirect,
} from "react-router-dom";
import { AuthContext } from "./Components/Context/AuthContext";
import { useContext } from "react";
import Home from "./Pages/HomePage/Home";
import PageNotFound from "./Pages/PageNotFound";

import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import CreateAccount from "./Pages/SignUp/CreateAccount";

function App() {
  const { user, isAuthenticated } = useContext(AuthContext);
  console.log(isAuthenticated);
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact>
            {isAuthenticated && user ? <Redirect to="/home-page" /> : <Login />}
          </Route>
          <Route path="/create-account">
            {isAuthenticated && user ? (
              <Redirect to="/home-page" />
            ) : (
              <CreateAccount />
            )}
          </Route>
          <Route exact path="/home-page">
            {isAuthenticated && user ? (
              <Home user={user} />
            ) : (
              <Redirect to="/" />
            )}
          </Route>
          <Route path="*">
            <PageNotFound />
          </Route>
        </Switch>
      </Router>
      <ToastContainer theme="colored" newestOnTop={true} />
    </div>
  );
}

export default App;
